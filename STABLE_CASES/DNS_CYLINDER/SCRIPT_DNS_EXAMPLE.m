%% Demonstration of how to do a DNS using StabFem
%
% In this tutorial script we show how to perform a DNS of the wake of a
% cylinder for Re = 60, allowing to observe the development and saturation
% of the well-known Von Karman vortex street.
% The DNS solver is based on Uzawa method using Cahouet preconditionner.
% The FreeFem++ code <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp> 
% is adapted from the one used in Jallas, Marquet & Fabre (PRE, 2017).

%%

verbosity = 0;
SF_Start;



%% Chapter 1 : generation of a mesh and base flow for Re=60
%
% If a base flow coming from a previous computation exists in the
% WORK/MESHES directory, we recover it
% (this trick is to save time)
bf = SF_Load('lastbaseflow'); 

%%
% Othewise  we recompute it. 

if isempty(bf)

%%
%
% We use the same procedure as in 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Mesh.m SF_Mesh.m>"CYLINDER_LINEAR.m" except that we use a full mesh
% instead of a half-mesh. First we build an initial mesh and progressively increase
% the Reynolds :
    ffmesh = SF_Mesh('Mesh_Cylinder_FullDomain.edp','problemtype','2D','symmetry','S');
    bf=SF_BaseFlow(ffmesh,'Re',1);
    bf=SF_BaseFlow(bf,'Re',10);
    bf=SF_BaseFlow(bf,'Re',60);
    
%%
% Then we readapt the mesh and use <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Split.m SF_Split.m> to split the mesh     
    bf = SF_Adapt(bf,'Hmax',5);
    bf = SF_Split(bf);
  
    
%%
% You now have a robust mesh well fitted to DNS. You may also try
% other adaptation strategies (adaptation to eigenmode, etc..). See other
% examples in the project !
% 
% Note that the initially generated mesh is not perfectly symmetric. In
% some cases (e.g. if you want to do DNS very close to the instability
% threshold Re ~ 47) it is desirable to have a perfectly symmetric mesh.
% A procedure to do that is to first generate a half-mesh, adapt it and 
% "mirror" it. An example on how to do this is available here :
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/CYLINDER_DNS/SCRIPT_DNS_WITHSYMMETRICMESH.m SCRIPT_DNS_WITHSYMMETRICMESH.m.m> 

end

%% Chapter 2 : Generation of a starting point for DNS using stability results
%
% We want to initialise the DNS with an initial condition 
% $u = u_b + \epsilon u_1$ where $u_b$ is the base flow, $u_1$ is the
% eigenmode, and $\epsilon = 0.01$.
% 
%

[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'sym','N'); % compute the eigenmode. 

startfield = SF_Add(bf,em,'coefs',[1 0.01]); % creates startfield = bf+0.01*em


%% Chapter 3 : Launch a DNS
%
% We do 5000 time steps and produce snapshots each 10 time steps.
% We use the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_DNS.m SF_DNS.m> 
% which launches the Freefem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp>

Nit = 8000; iout = 20;dt = 0.02;
[DNSstats,DNSfields] =SF_DNS(startfield,'Re',60,'Nit',Nit,'dt',dt,'iout',iout)


%% Chapter 4 : plotting the results and generating a movie
% 

%% 
% Here is how to generate a movie
h = figure;
filename = 'html/DNS_Cylinder_Re60.gif';
for i=1:length(DNSfields)
    SF_Plot(DNSfields(i),'vort','xlim',[-2 10],'ylim',[-3 3 ],'colorrange',[-3 3],...
        'title',['t = ',num2str((i-1)*dt)],'boundary','on','bdlabels',2);
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<DNS_Cylinder_Re60.gif>>
%

%% 
% We now plot the lift force as function of time

figure(15);
subplot(2,1,1);
plot(DNSstats.t,DNSstats.Fy);title('Lift force as function of time');
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstats.t,DNSstats.Fx);title('Drag force as function of time');
xlabel('t');ylabel('Fx');


%%
% Now we plot the pressure and vorticity along the surface
alpha = linspace(-pi,pi,201);
Xsurf = .501*cos(alpha);
Ysurf = .501*sin(alpha);
Psurf = SF_ExtractData(DNSfields(end),'p',Xsurf,Ysurf);
Omegasurf = SF_ExtractData(DNSfields(end),'vort',Xsurf,Ysurf);
figure(16);
subplot(2,1,1);title('Pressure along the surface P(r=a,\theta) at final time step')
plot(alpha,Psurf);
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);title('Vorticity along the surface \omega(r=a,\theta) at final time step')
plot(alpha,Omegasurf);
xlabel('\theta');ylabel('\omega_z(r=a,\theta)');

%% APPENDIX : how to do a DNS starting from a previous snapshot
%
% Suppose we want to restart the previous DNS and do another 100 time steps
% There are two possible syntaxes :
%
% 1/ 
% 
 DNSfieldlast = DNSfields(end);
 [DNSstats2,DNSfields2] = SF_DNS(DNSfieldlast,'Re',60,'Nit',100,'dt',dt,'iout',iout)

%%
% In this case [DNSstat2, DNSfields2] will only contain results from the restarted computation 
%
 
%%
% 2/ 
% 
  
[DNSstats3,DNSfields3] = SF_DNS(DNSfields,'Re',60,'Nit',100,'dt',dt,'iout',iout,...
                        'PreviousStats',DNSstats)

%%
% In this case [DNSstat, DNSfields] will append the result from the restarted computation
% to the ones coming from the previous computation(s)


