%> @file SOURCES_MATLAB/SF_core_generateopts.m
%> @brief Matlab function helping the user generating an option file.
%>
%> Usage: SF_core_generateopts()
%>
%> Options are placed in a global variable "sfopts" and saved in a
%> stabfem.opts file.
%>
%> sfopts.verbosity: verbosity level
%> sfopts.platform: OS platform (pc, mac or linux)
%> sfopts.interpreter: 'matlab' or 'octave' 
%> sfopts.gitavailable: is git executable available?
%> sfopts.gitrepository: is current stabfem install a git repository?
%> sfopts.sfroot: path to stabfem root
%> sfopts.ffroot: path to freefem binaries
%> sfopts.sfplot: are plot by StabFem enabled?
%> sfopts.ffplot: are plot by FreeFem enabled? (requires ffglut)
%> sfopts.ffglut: path to ffglut
%> sfopts.workdir: path to workdir
%>
%> @author Maxime Pigou
%> @version 1.0
%> @date 25/10/2018 Start writing version 1.0
function SF_core_generateopts()
% Function assisting the user in the option file definition.
global sfopts

sfopts = struct();
sfopts.verbosity = uint8(4);
SF_core_log('n', 'Verbosity is set to Notice (3) during options generation.');

% -- Detecting platform --
if ispc
    sfopts.platform = 'pc';
elseif isunix && ismac
    sfopts.platform = 'mac';
elseif isunix
    sfopts.platform = 'linux';
end
SF_core_log('n', sprintf('Detected platform: %s', sfopts.platform));

% -- Detecting whether MATLAB or OCTAVE --
sfopts.isoctave = logical(exist('OCTAVE_VERSION', 'builtin'));

% -- Detecting GIT installation --
sfopts.gitavailable = false;
sfopts.gitrepository = false;

[s,~] = system('git --version');
if s==0
    SF_core_log('n', 'Git is available on this platform.');
    sfopts.gitavailable = true;
end

if sfopts.gitavailable
    [s,~] = system('git rev-parse --is-inside-work-tree');
    if s==0
        SF_core_log('n', 'StabFem installation is located in a Git repository.');
        sfopts.gitrepository = true;
    else
        SF_core_log('n', 'StabFem installation is not located in a git repository.');
    end
end

% -- Obtaining Stabfem root directory --
sfroot = '';
if sfopts.gitavailable
    [s,t] = system('git rev-parse --show-toplevel');
    if s==0
        sfroot = t(1:end-1);
    end
else
    sfroot = pwd;
end
sfopts.sfroot=[];
while isempty(sfopts.sfroot)
    t=input(sprintf('Please enter StabFem root folder [%s]: ',sfroot),'s');
    if isempty(t)
        t = sfroot;
    end
    if exist(t,'dir')==7
        sfopts.sfroot = t;
    else
        SF_core_log('w','Invalid directory');
    end
end


addpath([sfopts.sfroot '/SOURCES_MATLAB']);
disp(['adding directory ''' sfopts.sfroot '/SOURCES_MATLAB '' to path']);
t=input(sprintf('OK to add permanently this direcoty to Matlab/Octave path ? [%s]: ','yes'),'s');
if isempty(t)||strcmpi(t,'yes')
    savepath;
end

% -- Locating FreeFem++ --
ffroot = '';
[s,t] = SF_core_syscommand('which', 'FreeFem++');
if s==0
    [ffroot,~,~] = fileparts(t);
%    if strcmp(ffroot(end-3:end),'/bin')
%        ffroot = ffroot(1:end-4);
%    else
%        ffroot = [ffroot '/../'];
%    end
% David : removing the  assumption that FreeFem++ is in a 'bin' subfolder 
end
sfopts.ffroot = [];
while isempty(sfopts.ffroot)
    t=input(sprintf('Please enter the folder containing the FreeFem++ executable [%s]: ',ffroot),'s');
    if isempty(t)
        t = ffroot;
    end
    if strcmp(t(end),'/')
       t = t(1:end-1);
    end
    if exist(t,'dir')==7
        sfopts.ffroot = t;
    else
        SF_core_log('w','Invalid directory');
    end
end

% -- Detecting advanced configuration elements --
% Detect FFglut and ask for usage => question about plots
% (Auto?) detect whether PETSc was used for freefem++ compilation?

% these ones are now displaced in SF_Start
sfopts.ffdir = [sfopts.sfroot '/SOURCES_FreeFem/']; 
sfopts.ffarg = '-nw -v 0'; 
sfopts.ffargDEBUG = '-v 10';


%TODO: improve setting these arguments
%sfopts.ffdatadir = './WORK/';
% -- Locating working directory --
% Default: './WORK' or '.\WORK'
