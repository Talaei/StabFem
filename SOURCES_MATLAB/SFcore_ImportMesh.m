function meshstruct = SFcore_ImportMesh(fileToRead1,varargin)
%
%  function SFcore_ImportMesh
%  Imports mesh generated by freefem
%  Usage :
% 1/ mesh=SFcore_ImportMesh(FileToRead1,[opt1,param1,...]) -> imports information for points & triangles
%
% Parameters : 
%   'problemtype' => Problem type (keyword to be used by the drivers)
%   Recognized values involve "2D", "2DMap", "AxiXR", ...
%   (NB in legacy versions this keyword was read in the file
%   "SF_Init.ff2m", it is not recommended to use this way anymore.)
%
% If present, the program will also read and import the following files :
%   (FileToRead1 is typically of the form "mesh.msh", if not change the file names accordingly).
% 
%  "mesh_connectivity.ff2m" 
%    => file containing connectivity data needed for plotting of P2 data
%       (created by macro SFWriteConnectivity) 
%
%  "mesh_mapping.ff2m" 
%   => file containing data needed to plot or postproceess results
%       involving coordinate mappings 
%       (created by macro SFWriteMapping)
%   
%   "mesh_surface.ff2m" 
%    => file containing data needed to plot and postprocess results 
%       for free-surface problems (free surface geometry and curvature,...)
%       (created by macro SFWriteSurface)
%
%    "mesh.ff2m"  (possibly obsolete)
%    => auxiliary data describing the mesh for post-processing
%                  (e.g. mesg density, etc...)
%
%   "SF_Init.ff2m" (possibly obsolete)
%    => physical dimensions of the domain 
%               (quantities which are not expected to change when remeshing)
%
%   "problemtype.ff2m"     
%    => keyword "problemtype"
%
%  History
%  This program originally adapted from FreeFem_to_matlab.m, copyright Julien Dambrine 2010 ;
%  initially called SFcore_ImportMesh modified by D. Fabre (2017) ; and redesigned in 2018 Nov. with help of Markus Chloros
%  Renamed into "SFcore_ImportMesh" on feb. 1 2019 to fit with new version
%
%

global sfopts


p = inputParser;
addParameter(p, 'problemtype','UNKNOWN');
parse(p, varargin{:})
% Check for the mesh file which should be in the ffdatadir directory
if (exist([sfopts.ffdatadir, fileToRead1],'file') == 2)
    fileToRead1 = [sfopts.ffdatadir, fileToRead1];
end

% Now reading mesh file using ffreadmesh from Markus
[meshstruct.points,meshstruct.bounds,meshstruct.tri,meshstruct.np,meshstruct.nbe,meshstruct.nt,meshstruct.labels]=ffreadmesh(fileToRead1);

% adding keywords problemtype and filename
meshstruct.problemtype = p.Results.problemtype;
meshstruct.filename = fileToRead1;
if (~isfield(meshstruct, 'meshtype'))
    meshstruct.meshtype = '2D';
end
meshstruct.datatype = 'mesh';

% Check if auxiliary files are present and if so import them
%First, checks of auxiliary files "mesh.ff2m" and "SF_Init.ff2m" are
%present ; if so imports them

[filepath, name, ~] = fileparts(fileToRead1);

% 1. mesh.ff2m (or similar name)
fileToRead2 = [filepath, '/', name, '.ff2m'];
if(exist(fileToRead2,'file'))
    mydisp(2, ['FUNCTION  SFcore_ImportMesh.m : reading complementary file' fileToRead2]);
    m2 = SFcore_ImportData(meshstruct,fileToRead2);
    meshstruct = SFcore_MergeStructures(meshstruct,m2);
else
     mydisp(5, ['FUNCTION  SFcore_ImportMesh.m : DO NOT FIND COMPLEMENTARY FILE ' fileToRead2]);
end

% 2. SFInit.ff2m
fileToRead2 = [filepath, '/SF_Init.ff2m'];
if(exist(fileToRead2,'file'))
    mydisp(5, ['FUNCTION  SFcore_ImportMesh.m : reading complementary file' fileToRead2]);
    m2 = SFcore_ImportData(fileToRead2);
    meshstruct = SFcore_MergeStructures(meshstruct,m2);
else
     mydisp(5, ['FUNCTION  SFcore_ImportMesh.m : DO NOT FIND COMPLEMENTARY FILE ' fileToRead2]);
end

% 3. mesh_connectivity.ff2m (or similar name)
fileToRead2 = [filepath, '/', name, '_connectivity.ff2m'];
if(exist(fileToRead2,'file'))
    mydisp(2, ['FUNCTION  SFcore_ImportMesh.m : reading complementary file' fileToRead2]);
    m2 = SFcore_ImportData(fileToRead2);
    meshstruct = SFcore_MergeStructures(meshstruct,m2);
else
     mydisp(5, ['FUNCTION  SFcore_ImportMesh.m : DO NOT FIND COMPLEMENTARY FILE ' fileToRead2]);
end

% 4. mesh_mapping.ff2m (or similar name)
fileToRead2 = [filepath, '/', name, '_mapping.ff2m'];
if(exist(fileToRead2,'file'))
    mydisp(2, ['FUNCTION  SFcore_ImportMesh.m : reading complementary file' fileToRead2]);
%    m2 = SFcore_ImportData(fileToRead2);
    m2 = SFcore_ImportData(meshstruct,fileToRead2);
    meshstruct = SFcore_MergeStructures(meshstruct,m2);
else
     mydisp(5, ['FUNCTION  SFcore_ImportMesh.m : DO NOT FIND COMPLEMENTARY FILE ' fileToRead2]);
end

% 5. mesh_surface.ff2m (or similar name)
fileToRead2 = [filepath, '/', name, '_surface.ff2m'];
if(exist(fileToRead2,'file'))
    mydisp(2, ['FUNCTION  SFcore_ImportMesh.m : reading complementary file' fileToRead2]);
    m2 = SFcore_ImportData(meshstruct,fileToRead2);
    meshstruct = SFcore_MergeStructures(meshstruct,m2);
else
     mydisp(5, ['FUNCTION  SFcore_ImportMesh.m : DO NOT FIND COMPLEMENTARY FILE ' fileToRead2]);
end

% 6. problemtype.ff2m 
fileToRead2 = [sfopts.ffdatadir, '/problemtype.ff2m'];
if(exist(fileToRead2,'file'))
    mydisp(2, ['FUNCTION  SFcore_ImportMesh.m : reading complementary file' fileToRead2]);
    m2 = SFcore_ImportData(meshstruct,fileToRead2);
    meshstruct.problemtype = m2.problemtype;
else
     mydisp(5, ['FUNCTION  SFcore_ImportMesh.m : DO NOT FIND COMPLEMENTARY FILE ' fileToRead2]);
end


%
% The remainder of this routine has to be rationalised...
%


%% change the field "datatype" to "problemtype" (to be rationalized ?)
%if (isfield(meshstruct, 'datatype')) % for retrocompatibility ; to be removed in future
%    error(['WARNING : in mesh.ff2m datatyle should be replaced by problemtype']);
%%    meshstruct.problemtype = meshstruct.datatype;
%%    meshstruct = rmfield(meshstruct, 'datatype');
%end

if (~isfield(meshstruct, 'meshgeneration')) % for retrocompatibility ; to be removed in future
    meshstruct.meshgeneration = findgeneration(fileToRead1);
    % initial mesh should be generation = 0
end


% if(strcmpi(meshstruct.meshtype,'2DMapped'))
%     mydisp(2,'Mapped mesh ; reading additional file for physical coordinates and mapping jacobians');
%     fileToRead4 = [sfopts.ffdatadir,'Mapping.ff2m'];
%     if(exist(fileToRead4))
%         m2 = SFcore_ImportData(meshstruct,fileToRead4);
%         %merge m2 and meshstruct
%         f = fieldnames(m2);
%         for i = 1:length(f)
%             if (~strcmpi(f{i},'filename'))&&(~strcmpi(f{i},'datatype'))&&(~strcmpi(f{i},'mesh'))
%                 meshstruct.(f{i}) = m2.(f{i});
%             end
%         end
%     end
% end
mydisp(2, ['END FUNCTION SFcore_ImportMesh.m'])
end
function generation = findgeneration(Filename)
% this function extracts the number in a filename with the form
% "mesh_adapt##.msh" or "mesh_stretch##.msh"
    underlineLocations = find((Filename=='_'));
    if(length(underlineLocations)==1)
        if(Filename(underlineLocations(1)+1)=='a')
            generation = str2double(Filename(underlineLocations(1)+6:end-4));
        elseif(Filename(underlineLocations(1)+1)=='s')
            generation = str2double(Filename(underlineLocations(1)+8:end-4));
        else
            generation = 0;
        end
    elseif(length(underlineLocations)>=1)
        if(Filename(underlineLocations(1)+1)=='a')
            generation = str2double(Filename(underlineLocations(1)+6:underlineLocations(2)-1));
        elseif(Filename(underlineLocations(1)+1)=='s')
            generation = str2double(Filename(underlineLocations(1)+8:underlineLocations(2)-1));
        else
            generation = 0;
        end
    else
        generation = 0;
    end
end    

