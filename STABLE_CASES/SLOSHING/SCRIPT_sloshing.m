%% Oscillation modes of a partially filled cylindrical container
%
% The aim of this tutorial program is to demonstrate how to use
% StabFem for capillary oscillations of static free-surface problem.
%
% The investigated situation corresponds to a partially filled cylindrical
% container with radius R. we note L the height of liquid along the
% vertical wall (including the meniscus), gamma the surface tension, theta 
% the contact angle and rhog the product between gravity acceleration and liquid density.
%

%% Initialization
close all;
addpath('../../SOURCES_MATLAB/');
verbosity = 2;ffdatadir = './WORK/';
SF_Start;

system('mkdir FIGURES');
figureformat = 'png';

%% Chapter 1 : Oscillation modes for the case of a flat surface
%

%%
% We first create a  mesh with rectangular shape (flat surface).
% See the Freefem++ program 
% < https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/SLOSHING/MeshInit_Vessel.edp MeshInit_Vessel.edp >
% to see how it works. 

R = 1; % radius of the vessel
L = 2; % Height of the vessel
density=50; % this is the mesh density in the initial mesh 
ffmesh = SF_Mesh('MeshInit_Vessel.edp','Params',[L density],'problemtype','3DFreeSurfaceStatic');

%%
%
% Here we set the physical parameters for this flat surface 
gamma = 0.002;rhog = 1;
ffmesh.gamma = gamma;
ffmesh.rhog = rhog;

%% CHAPTER 2 : Eigenvalue computation for a flat surface, free condition, INVISCID
%
%
%
% In this case there is a classical theoretical solution.
% Noting $m$ the azimuthal wavenumber and $n$ radial wavenumber (number of
% nodal circles), we have 
% $\omega_{m,n}  = \sqrt{ k_{m,n} g + \gamma / \rho k_{m,n}^3}$ 
% with $k_{m,n} R = j'_{m,n}$ 
% ($j'_{m,n}$ being the nth zero of the bessel funcion $J_m'$)
%
% First we compute this theoretical solution for m=1 and n =(1,2,3) :

j1p(1) = 1.8412; j1p(2) = 5.3314; j1p(3) = 8.5363; % zeros of j1'
k = j1p(:)/R;
g= 1;gamma=0.002;rho=1;H=2;

disp( 'frequencies of m = 1 modes : ');
evTheory = sqrt( (g*k+gamma*k.^3).*tanh(k*H))

%%
% 
% We now compute the eigenvalues using the generic driver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Stability.m SF_Stability.m>
% which invokes the FreeFem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/StabAxi_FreeSurface_Potential.edp StabAxi_FreeSurface_Potential.edp>


[evm1FLATFREE,emm1] =  SF_Stability(ffmesh,'nev',10,'m',1,'shift',2.1i,'typestart','freeV','typeend','axis');
evm1 = evm1FLATFREE;

%%
% Note that we have used a shift centered on the (m,n) = (1,2) mode. 
% with this choice the modes (m,n) = (1,1), (1,2) and (1,3) will be found with index (2,1,3)
%
% Here we compare the results with the theory :

error1 = abs(imag(evm1FLATFREE(2))/evTheory(1)-1)+abs(imag(evm1FLATFREE(1))/evTheory(2)-1)+abs(imag(evm1FLATFREE(3))/evTheory(3)-1)


%% Chapter 2b : plot the results

%%
% 
% Secondly plot the structure of the modes 
% (we plot the imaginary part of the velocity potential along with the free
% surface deformation).
%
% This is the first mode :
figure;SF_Plot(emm1(2),'phi.im','title',{'Mode (m,n)= (1,1)',['freq = ',num2str(imag(evm1(1)))]},'symmetry','YA','xlim',[-1 1]);
hold on;SF_Plot_ETA(emm1(2),'symmetry','YA');

%%
% ploting the first three modes

figure(2);suptitle('m=1 sloshing modes : Horizontal surface, free condition');hold on;
subplot(1,3,1);
SF_Plot(emm1(2),'phi.im','title',{'Mode (m,n)= (1,1)',['freq = ',num2str(imag(evm1(2)))]},'symmetry','YA','xlim',[-1 1]);
hold on;SF_Plot_ETA(emm1(2),'symmetry','YA');
subplot(1,3,2);
SF_Plot(emm1(1),'phi.im','title',{'Mode (m,n)= (1,2)',['freq = ',num2str(imag(evm1(1)))]},'symmetry','YA','xlim',[-1 1]);
hold on;SF_Plot_ETA(emm1(1),'symmetry','YA');
subplot(1,3,3);
SF_Plot(emm1(3),'phi.im','title',{'Mode (m,n)= (1,3)',['freq = ',num2str(imag(evm1(3)))]},'symmetry','YA','xlim',[-1 1]);
hold on;SF_Plot_ETA(emm1(3),'symmetry','YA');
pos = get(gcf,'Position'); pos(3)=pos(4)*2.6;set(gcf,'Position',pos); % resize aspect ratio

%%
% Note that the option ('symmetry','YA') allows to plot the structure in
% the symmetrized range r = [-1,1] taking into account the antisymmetry of phi (for m=1).  


pause(1);

%% CHAPTER 3 : eigenvalues for flat surface, pined condition

disp('### Second test : flat surface, pined condition')

[evm1FLATPINED,emm1] =  SF_Stability(ffmesh,'nev',10,'m',1,'shift',2.1i,'typestart','pined','typeend','axis');
evTheory = [1.4444    2.4786    3.2671]; % There is a paper by Miles with analytical solution... 
error2 = abs(imag(evm1FLATPINED(2))/evTheory(1)-1)+abs(imag(evm1FLATPINED(1))/evTheory(2)-1)+abs(imag(evm1FLATPINED(3))/evTheory(3)-1)


%% 
% The figures are very similar to the previous case, uncomment the next
% lines to see them !

%figure(1);
%plot(imag(evm1),0*real(evm1),'b+');hold on;
%legend('flat, pined');
%figure(3);suptitle('m=1 sloshing modes : Horizontal surface, H/R = 2, Bo = 500, pined condition');hold on;
%subplot(1,3,1);
%SF_Plot(emm1(2),'phi.im','title',{'Mode (m,n)= (1,1)',['freq = ',num2str(imag(evm1(2)))]},'symmetry','YA');hold on;SF_Plot_ETA(emm1(2),'symmetry','YA');
%subplot(1,3,2);
%SF_Plot(emm1(1),'phi.im','title',{'Mode (m,n)= (1,2)',['freq = ',num2str(imag(evm1(1)))]},'symmetry','YA');hold on;SF_Plot_ETA(emm1(1),'symmetry','YA');
%subplot(1,3,3);
%SF_Plot(emm1(3),'phi.im','title',{'Mode (m,n)= (1,3)',['freq = ',num2str(imag(evm1(3)))]},'symmetry','YA');hold on;SF_Plot_ETA(emm1(3),'symmetry','YA');
%pos = get(gcf,'Position'); pos(3)=pos(4)*2.6;set(gcf,'Position',pos); % resize aspect ratio
%pause(1);



%% Chapter 4 : meniscus (theta = 45?), free conditions

% This case is the same as in Viola, Gallaire & Brun

% We first compute equilibrium shape and corresponding mesh.
% The equilibrium shape is the solution of the Young-Laplace equation :
% 
% $p + \rho g z + \gamma K = 0$ along the free surface 
%
% We solve this equation using an iterative method implying a mesh
% deformation at each step.
% The method is described in this (unfinished and unpublished) paper :
% <https://github.com/erbafdavid/Meniscus/blob/master/BLOCNOTE_CURVATURE_FREQUENCIES/JFM_Menisci.pdf JFM_Menisci.pdf>
%
% A simpler matlab implementation of the same algorithm can be found here:
% <https://github.com/erbafdavid/Meniscus/blob/master/meniscus.m meniscus.m>

gamma = 0.002;
thetaE = pi/4; % this is the contact angle used in Viola et al.
hmeniscus = sqrt(2*gamma*(1-sin(thetaE))); % this is the expected height of the meniscus (valid for large Bond)
P = -ffmesh.rhog*hmeniscus; % pressure in the liquid at z=0 (altitude of the contact line) ; 
% the result will be to lower the interface by this ammount 

%%
% Starting with the previously computed "flat surface" mesh as an initial "guess", 
% we use the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Mesh_Deform.m SF_Mesh_Deform.m> 
% to compute a mesh with a free surface satisfying the Young-Laplace
% solution. 
% The driver launches the FreeFem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/Newton_Axi_FreeSurface_Static.edp Newton_Axi_FreeSurface_Static.edp>
% .

ffmesh = SF_Mesh_Deform(ffmesh,'P',P,'gamma',gamma,'rhog',rhog,'typestart','pined','typeend','axis');

%%
% We plot this mesh :

figure;SF_Plot(ffmesh,'mesh','symmetry','ys');


%%
% A few statistics about the free surface shape :
% 
zR = ffmesh.zsurf(1) % altitude of the contact line
z0 = ffmesh.zsurf(end) % altitude of the center
Vol0 = ffmesh.Vol % volume
alphastart = ffmesh.alpha(1)*180/pi % this should be 225 degrees (angle with respect to vertical = 45 degrees)

%%
% We now compute m=1 eigenvalues using this mesh :

[evm1MENISCUSFREE,emm1] =  SF_Stability(ffmesh,'nev',10,'m',1,'shift',2.1i,'typestart','freeV','typeend','axis');

evTheory = [1.3587    2.3630    3.1118]; % the values are compatible with results from viola et al.
error3 = abs(imag(evm1MENISCUSFREE(2))/evTheory(1)-1)+abs(imag(evm1MENISCUSFREE(1))/evTheory(2)-1)+abs(imag(evm1MENISCUSFREE(3))/evTheory(3)-1)

%%
% We plot these modes



figure(4);suptitle('m=1 sloshing modes : Meniscus (45?), H/R = 2, Bo = 500, free condition');hold on;
subplot(1,3,1);
SF_Plot(emm1(2),'phi.im','title',{'Mode (m,n)= (1,1)',['freq = ',num2str(imag(evm1(2)))]},'symmetry','YA','xlim',[-1 1]);
hold on;SF_Plot_ETA(emm1(2),'Amp',0.05,'symmetry','YA');
subplot(1,3,2);
SF_Plot(emm1(1),'phi.im','title',{'Mode (m,n)= (1,2)',['freq = ',num2str(imag(evm1(1)))]},'symmetry','YA','xlim',[-1 1]);
hold on;SF_Plot_ETA(emm1(1),'Amp',0.05,'symmetry','YA');
subplot(1,3,3);
SF_Plot(emm1(3),'phi.im','title',{'Mode (m,n)= (1,3)',['freq = ',num2str(imag(evm1(3)))]},'symmetry','YA','xlim',[-1 1]);
hold on;SF_Plot_ETA(emm1(3),'Amp',0.05,'symmetry','YA');
pos = get(gcf,'Position'); pos(3)=pos(4)*2.6;set(gcf,'Position',pos); % resize aspect ratio


%%
% check if boundary condition is correctly verified

%figure(51);title('eta (plain), - d eta /ds + K0a cot(alpha) eta (dashed), limit (dotted)');
%DetaDs = diff(emm1(1).eta)./diff(ffmesh.S0);
%plot(ffmesh.xsurf,real(emm1(1).eta),'-'); hold on; 
%plot((ffmesh.xsurf(1:end-1)+ffmesh.xsurf(2:end))/2,DetaDs,'--');
%plot(ffmesh.xsurf,-ffmesh.K0a.*cot(ffmesh.alpha).*(abs(cot(ffmesh.alpha))<1e2).*emm1(1).eta);


%% Chapter 5 : meniscus (theta = 45?), VISCOUS, m=1
%
% NB in the viscous case, the driver  <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_Stability.m SF_Stability.m>
% invokes the FreeFem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/StabAxi_FreeSurface_Viscous.edp StabAxi_FreeSurface_Viscous.edp>

nu = 1e-4;
m=1;sym =  'YA';% YS if m is even, YA if m is odd
[evm1VISCOUS,emm1] =  SF_Stability(ffmesh,'nev',10,'m',1,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis');

%%
% At this stage it is useful to adapt the mesh to capture the thin boundary layer

 ffmesh = SF_Adapt(ffmesh,emm1(3),'Hmax',.1) ;
 figure(7);SF_Plot(ffmesh,'mesh');
%%
% Now recompute the modes

[evm1VISCOUS,emm1] =  SF_Stability(ffmesh,'nev',10,'m',1,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis');

%%
% Plot the results

figure(5);
suptitle(['Sloshing modes : Meniscus (45?), H/R = 2, Bo = ' num2str(1/gamma) '; Oh = ' num2str(nu)  '; m = ' num2str(m) ]);hold on;
subplot(1,3,1);
SF_Plot(emm1(2),'uz1.im','title',{'Mode (m,n)= (1,1)',['\omega_r = ',num2str(imag(evm1(1))),', \omega_i = ',num2str(real(evm1(1)))]},'symmetry',sym,'xlim',[-1 1]);hold on;
SF_Plot_ETA(emm1(2),'Amp',0.05,'symmetry',sym);xlim([-1 1]);ylim([-2,.5]);
subplot(1,3,2);
SF_Plot(emm1(1),'uz1.im','title',{'Mode (m,n)= (1,2)',['\omega_r = ',num2str(imag(evm1(2))),', \omega_i = ',num2str(real(evm1(2)))]},'symmetry',sym,'xlim',[-1 1]);hold on;
SF_Plot_ETA(emm1(1),'Amp',0.05,'symmetry',sym);xlim([-1 1]);ylim([-2,.5]);
subplot(1,3,3);
SF_Plot(emm1(3),'uz1.im','title',{'Mode (m,n)= (1,3)',['\omega_r = ',num2str(imag(evm1(3))),', \omega_i = ',num2str(real(evm1(3)))]},'symmetry',sym,'xlim',[-1 1]);hold on;
SF_Plot_ETA(emm1(3),'Amp',0.05,'symmetry',sym);xlim([-1 1]);ylim([-2,.5]);
pos = get(gcf,'Position'); pos(3)=pos(4)*2.6;set(gcf,'Position',pos); % resize aspect ratio

figure(6);
SF_Plot(emm1(2),'uz1.im','title',{'Mode (m,n)= (1,1)',['\omega_r = ',num2str(imag(evm1(1))),', \omega_i = ',num2str(real(evm1(1)))]},'symmetry',sym,'xlim',[-1 1]);hold on;
SF_Plot_ETA(emm1(2),'Amp',0.05,'symmetry',sym);xlim([-1 1]);ylim([-2,.5]);
title('Sloshing mode in a cylindrical container (viscous problem)')




figure(1);
plot(imag(evm1FLATFREE),real(evm1FLATFREE),'g+');hold on;
plot(imag(evm1FLATPINED),real(evm1FLATPINED),'ro');hold on;
plot(imag(evm1MENISCUSFREE),real(evm1MENISCUSFREE),'bx');hold on;
plot(imag(evm1VISCOUS),real(evm1VISCOUS),'k*');hold on;
xlabel('omega_r');ylabel('omega_i');
legend('flat, free', 'flat, pined','meniscus, free','meniscus, pined, viscous' );
xlim([0 6]);ylim([-.1 .05]);

%% APPENDIX B : meniscus (theta = 45?), free conditions m=0
% (uncomment the following to see how it works...)
% 

%his case is the same as in Viola, Gallaire & Brun
% 
% nu = 1e-3;
% m=0;sym =  'YS';% YS if m is even, YA if m is odd
% [evm1,emm1] =  SF_Stability(ffmesh,'nev',10,'m',m,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis');
% 
% 
% %evTheory = [1.3587    2.3630    3.1118]; 
% %error4 = abs(imag(evm1(2))/evTheory(1)-1)+abs(imag(evm1(1))/evTheory(2)-1)+abs(imag(evm1(3))/evTheory(3)-1)
% 
% figure(6);hold off;
% suptitle(['Sloshing modes : Meniscus (45?), H/R = 2, Bo = ' num2str(1/gamma) '; Oh = ' num2str(nu)  '; m = ' num2str(m) ]);hold on;
% subplot(1,3,1);
% SF_Plot(emm1(1),'uz1.im','title',{'Mode (m,n)= (0,1)',['\omega_r = ',num2str(imag(evm1(1))),', \omega_i = ',num2str(real(evm1(1)))]},'symmetry',sym);
% hold on;SF_Plot_ETA(emm1(1),'Amp',0.05,'symmetry','YS');xlim([-1 1]);ylim([-2,.5]);
% subplot(1,3,2);
% SF_Plot(emm1(2),'uz1.im','title',{'Mode (m,n)= (0,2)',['\omega_r = ',num2str(imag(evm1(2))),', \omega_i = ',num2str(real(evm1(2)))]},'symmetry',sym);
% hold on;SF_Plot_ETA(emm1(2),'Amp',0.05,'symmetry','YS');xlim([-1 1]);ylim([-2,.5]);
% subplot(1,3,3);
% SF_Plot(emm1(3),'uz1.im','title',{'Mode (m,n)= (0,3)',['\omega_r = ',num2str(imag(evm1(3))),', \omega_i = ',num2str(real(evm1(3)))]},'symmetry',sym);
% hold on;SF_Plot_ETA(emm1(3),'Amp',0.05,'symmetry','YS');xlim([-1 1]);ylim([-2,.5]);
% pos = get(gcf,'Position'); pos(3)=pos(4)*2.6;set(gcf,'Position',pos); % resize aspect ratio
% 
% 
% 
% 
% 

%% APPENDIX C : meniscus (theta = 45?), free conditions m=2
% 
% 

% This case is the same as in Viola, Gallaire & Brun
% nu = 1e-3;
% [evm1,emm1] =  SF_Stability(ffmesh,'nev',10,'m',2,'nu',nu,'shift',2.1i,'typestart','freeV','typeend','axis');
% m=2;sym =  'YS';% YS if m is even, YA if m is odd
% 
% %evTheory = [1.3587    2.3630    3.1118]; 
% %error4 = abs(imag(evm1(2))/evTheory(1)-1)+abs(imag(evm1(1))/evTheory(2)-1)+abs(imag(evm1(3))/evTheory(3)-1)
% 
% figure(7);hold off;
% suptitle(['Sloshing modes : Meniscus (45?), H/R = 2, Bo = ' num2str(1/gamma) '; Oh = ' num2str(nu)  '; m = ' num2str(m) ]);hold on;
% subplot(1,3,1);
% SF_Plot(emm1(1),'uz1.im','title',{'Mode (m,n)= (2,1)',['\omega_r = ',num2str(imag(evm1(1))),', \omega_i = ',num2str(real(evm1(1)))]},'symmetry',sym);
% hold on;SF_Plot_ETA(emm1(1),'Amp',0.05,'symmetry','YS');xlim([-1 1]);ylim([-2,.5]);
% subplot(1,3,2);
% SF_Plot(emm1(2),'uz1.im','title',{'Mode (m,n)= (2,2)',['\omega_r = ',num2str(imag(evm1(2))),', \omega_i = ',num2str(real(evm1(2)))]},'symmetry',sym);
% hold on;SF_Plot_ETA(emm1(2),'Amp',0.05,'symmetry','YS');xlim([-1 1]);ylim([-2,.5]);
% subplot(1,3,3);
% SF_Plot(emm1(3),'uz1.im','title',{'Mode (m,n)= (2,3)',['\omega_r = ',num2str(imag(evm1(3))),', \omega_i = ',num2str(real(evm1(3)))]},'symmetry',sym);
% hold on;SF_Plot_ETA(emm1(3),'Amp',0.05,'symmetry','YS');xlim([-1 1]);ylim([-2,.5]);
% pos = get(gcf,'Position'); pos(3)=pos(4)*2.6;set(gcf,'Position',pos); % resize aspect ratio









