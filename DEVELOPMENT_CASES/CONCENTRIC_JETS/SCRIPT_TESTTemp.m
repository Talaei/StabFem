
run('../../SOURCES_MATLAB/SF_Start.m');
verbosity=1000;

%% first generate a mesh and base flow

ffmesh = SF_Mesh('Mesh_ConcentricJets.edp','params',[.5 1 1 100 15 1 1],'problemtype','AxiXRCOMPLEX');
% parameters are [R1, LS , D2, Xmax, Rmax Umax1 Umax2 ]
SF_Plot(ffmesh,'mesh');

Params = [10000000 1e30  1 0.3 1000000 1e30]; % Lm, LA, LC, gammac, yA, yB
ffmesh = SF_SetMapping(ffmesh,'mappingtype','jet','mappingparams',Params); 


bf = SF_BaseFlow(ffmesh,'Re',1,'Type','NEW');
bf = SF_Adapt(bf);


ReTab = [10 30 50 100 200 500 600 700 800 1000 1200 1400 1600 1700 1800]
for Re = ReTab(end)
    bf = SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf,'Hmax',1);
end

Params = [50 1e30  70 0.3 15 1e30]; % Lm, LA, LC, gammac, yA, yB
bf = SF_SetMapping(bf,'mappingtype','jet','mappingparams',Params); 
n = 20;
range = linspace(0.05,1.0,n);
evList = [];
emList = [];

for i=[1:n]
    [ev,em] = SF_Stability(bf,'Re',Re,'m',1,'shift',0.05+range(i)*1i,'nev',5);
    evList = [ev, evList];
    emList = [em, emList];
end

figure; plot(real(evList),imag(evList),'o');
hold on;
plot(linspace(0,0,2),linspace(0,1,2))

%% Considering gamma_c = 0.3
unstable = [55,59,60];
evListRefined = [];
emListRefined = [];
for i=[7:10]
    [ev,em] = SF_Stability(bf,'Re',Re,'m',1,'shift',range(i)*1i,'nev',5);
    evListRefined = [ev, evListRefined];
    emListRefined = [em, emListRefined];
end

figure;  SF_Plot(em(1),'ux','xlim',[0 100],'ylim',[0 15]);
[ev,em] = SF_Stability(bf,'Re',Re,'m',1,'shift',0.0133+0.52i,'nev',5);
% Plot baseflow/mode
% figure;  SF_Plot(bf,'ux','xlim',[0 50],'ylim',[0 5]);
% figure;  SF_Plot(em(1),'ux','xlim',[0 100],'ylim',[0 15]);

% 
% %% Figures
% figure;  SF_Plot(bf,'ux','xlim',[0 5],'ylim',[0 5]);
% hold on; SF_Plot(bf,{'ux','ur'},'xlim',[0 5],'ylim',[0 5]);
