function handle = SF_Plot(FFdata, varargin)
%  function SF_Plot
%  plots a data field imported from freefem.
%  This function is part of the StabFem project by D. Fabre & coworkers.
%
%   This version of SF_Plot is based on ffpdeplot developed by chloros2
%   as an Octave-compatible alternative to pdeplot from toolbox pdetools
%   (https://github.com/samplemaker/freefem_matlab_octave_plot)
%
%
%  Usage :
%  1/  handle=plotFF(ffdata,'mesh'[,PARAM,VALUE,..]);  
%           To plot a mesh
%  2/  handle=plotFF(ffdata,'field'[,PARAM,VALUE,..]); 
%           To plot isocontours of a P1 or P2 field specified by its name
%  2b/ handle=plotFF(ffdata,'field.re'[,PARAM,VALUE,..] ); 
%           To plot isocontours of a complex field (specify '.re' or '.im')
%  2c/ handle=plotFF(ffdata,data,[,PARAM,VALUE,..] ); 
%           To plot isocontours of a field transmitted as a vector (dimension must be consistent)
%  3/ handle=plotFF(ffdata,{'field1','field2'} [,PARAM,VALUE,..] );
%           To plot quiver plots (vector field with two components)
%
%  (NB :  4/ handle=plotFF(mesh) also works as an alternative syntax to 1/ but his syntax is not recommended anymore)  
%
%  FFdata is the structure containing the data to plot 
%       a valid 'dataset' structure is expected (with a mesh as a field of this structure)
%       but a 'mesh' structure can also be tranmitted directly.
%
%   [PARAM,VALUE] are any couple of name/value parameter accepted by
%   ffpdeplot. 
%
%   The list of accepted parameters are the same as accepted by ffpdeplot,
%   plus two specific ones : 'symmmetry' and 'logsat'.
%   NB : parameter 'colomap' accepts  a few custom ones including 'redblue' and 'ice'. 
%
%   Here is the list of parameters accepted by pdeplot :
%
%      Parameter       Value
%      'VhSeq'        Finite element connectivity
%                        FreeFem++ macro definition
%      'XYData'       PDE data used to create the plot
%                        FreeFem++ macro definition
%      'XYStyle'      Coloring choice
%                        'interp' (default) | 'off'
%      'ZStyle'       Draws 3D surface plot instead of flat 2D Map plot
%                        'continuous' | 'off' (default)
%      'ColorMap'     ColorMap value or matrix of such values
%                        'off' | 'cool' (default) | colormap name | three-column matrix of RGB triplets
%      'ColorBar'     Indicator in order to include a colorbar
%                        'on' (default) | 'off' | 'northoutside' ...
%      'CBTitle'      Colorbar Title
%                        (default=[])
%      'ColorRange'   Range of values to adjust the colormap thresholds
%                        'off' | 'minmax' (default) | 'centered' | 'cropminmax' | 'cropcentered' | [min,max]
%      'Mesh'         Switches the mesh off / on
%                        'on' | 'off' (default)
%      'MColor'       Color to colorize the mesh
%                        'auto' (default) | RGB triplet | 'r' | 'g' | 'b'
%      'RLabels'      Meshplot of specified regions
%                        [] (default) | [region1,region2,...]
%      'RColors'      Colorize regions with a specific color (linked to 'RLabels')
%                        'b' (default) | three-column matrix of RGB triplets
%      'Boundary'     Shows the domain boundary / edges
%                        'on' | 'off' (default)
%      'BDLabels'     Draws boundary / edges with a specific label
%                        [] (default) | [label1,label2,...]
%      'BDColors'     Colorize boundary / edges with a specific color (linked to 'BDLabels')
%                        'r' (default) | three-column matrix of RGB triplets
%      'BDShowText'   Shows the labelnumber on the boundary / edges
%                        'on' | 'off' (default)
%      'BDTextSize'   Size of labelnumbers on the boundary / edges
%                        scalar value greater than zero
%      'BDTextWeight' Character thickness of labelnumbers on the boundary / edges
%                        'normal' (default) | 'bold'
%      'Contour'      Isovalue plot
%                        'off' (default) | 'on'
%      'CStyle'       Contour plot style
%                        'solid' (default) | 'dashed' | 'dashedneg'
%      'CColor'       Isovalue color (can be monochrome or flat)
%                        'flat' | [0,0,0] (default) | RGB triplet, three-element row vector | 'r' | 'g' | 'b'
%      'CLevels'      Number of isovalues used in the contour plot
%                        (default=10)
%      'CGridParam'   Number of grid points used for the contour plot
%                        'auto' (default) | [N,M]
%      'Title'        Title
%                        (default=[])
%      'XLim'         Range for the x-axis
%                        'minmax' (default) | [min,max]
%      'YLim'         Range for the y-axis
%                        'minmax' (default) | [min,max]
%      'ZLim'         Range for the z-axis
%                        'minmax' (default) | [min,max]
%      'DAspect'      Data unit length of the xy- and z-axes
%                        'off' | 'xyequal' (default) | [ux,uy,uz]
%      'FlowData'     Data for quiver plot
%                        FreeFem++ point data | FreeFem++ triangle data
%      'FColor'       Color to colorize the quiver arrows
%                        'b' (default) | RGB triplet | 'r' | 'g'
%      'FGridParam'   Number of grid points used for quiver plot
%                        'auto' (default) | [N,M]
%
% ADDITIAL PARAMETERS NOT RECOGNIZED BY FFPDEPLOT :
%
%       'symmetry'  symmetry property of the flow to plot
%                       'no' (default) | 'YS' (symmetric w.r.t. Y axis) | 'YA' (antisymmetric w.r.t. Y axis) | 'XS' | 'XA' 
%                                      | 'XM' (mirror image w/r to X axis) | 'YM'  
%       'logsat'    use nonlinearly scaled colorange using filter function f_S
%                   colorange is linear when |value|<S and turns into logarithmic when |value|>S  
%                   (use this option to plot fields with strong spatial amplifications)
%                   NB : is S = 0 the colorrange is purely logarithmic
%                   -1 (default, disabled) | S
%     Notes :


global ff ffdir ffdatadir sfdir verbosity


% first check if 'colormap' is a custom one 
%   (a few customs are defined at the bottom of this function)
for i=1:length(varargin)-1
    if(strcmp(lower(varargin{i}),'colormap'))
        switch(lower(varargin{i+1}))
            case('redblue')
                varargin{i+1} = redblue(); % defined at the bottom
            case('french')
                varargin{i+1} = french();
            case('ice')
                varargin{i+1} = ice();
            case('fire')
                varargin{i+1} = fire();
            case('seashore')
                varargin{i+1} = seashore();
            case('dawn')
                varargin{i+1} = dawn();
                %otherwise varargin{i+1} should be a standard colormap
        end    
    end   
end

% check if 'contour' is part of the parameters and recovers its value
contourval='off';
for i=1:length(varargin)-1
      if(strcmp(varargin{i},'contour'))
           icontour = i;
           contourval = varargin{i+1};
      end    
end

% check if 'xystyle' is part of the parameters and recovers it
xystyle = 'on';
for i=1:length(varargin)-1
      if(strcmp(varargin{i},'xystyle'))
           xystyle = varargin{i+1};
      end    
end
if (strcmpi(xystyle,'off')==1)
       varargin = { varargin{:}, 'colorrange','off','colorbar','off','colormap','off'} ;
end

% check if 'symmetry' is part of the parameters and recovers it
symmetry = 'no';
for i=1:length(varargin)-1
      if(strcmp(varargin{i},'symmetry'))
           isymmetry = i;
           symmetry = varargin{i+1};
      end    
end
if (strcmp(symmetry,'no')~=1)
       varargin = { varargin{1:isymmetry-1} ,varargin{isymmetry+2:end}} ;
       if(strcmpi(symmetry,'ya')||strcmpi(symmetry,'xa'))
            varargin = [ varargin 'colorrange','cropcenter'];
       end
end

% check if 'logsat' is part of the parameters and recovers it
logscaleS = -1;
for i=1:length(varargin)-1
      if(strcmp(varargin{i},'logsat'))
           ilogscale = i;
           logscaleS = varargin{i+1};
           mydisp(2,['using colorrange with logarithmic saturation ; S = ',num2str(logscaleS)]);
      end    
end
if (logscaleS~=-1)
       varargin = { varargin{1:ilogscale-1} ,varargin{ilogscale+2:end}} ;
end

% check if 'amp' is part of the parameters and recovers it
iAmpplot = 0;Ampplot = 1;
for i=1:length(varargin)-1
      if(strcmpi(varargin{i},'amp'))
           iAmpplot = i;
           Ampplot = varargin{i+1};
           mydisp(2,['using amplitude ; A = ',num2str(Ampplot)]);
      end    
end
if (iAmpplot~=0)
       varargin = { varargin{1:iAmpplot-1} ,varargin{iAmpplot+2:end}} ;
end



%%% prepares to invoke ffpdeplot...
if (mod(nargin, 2) == 0) 
% plot mesh in double-entry mode : first parameter is a dataset, second parameter can be : 
%   a/ the name of a field (for contour plots)
%   b/ a cell-array with two names (for quiver plots)
%   c/ the keyword 'mesh' (to plot the mesh)

    if strcmpi(FFdata.datatype,'mesh')
        mesh = FFdata;
    else
        mesh = FFdata.mesh;
    end
    
    field1 = varargin{1};
    varargin = {varargin{2:end}};
     if(isfield(mesh,'xlim'))
        varargin = {varargin{:}, 'xlim', mesh.xlim};
    end
    if(isfield(mesh,'ylim'))
        varargin = {varargin{:}, 'ylim', mesh.ylim};
    end
    if (~iscell(field1)==1&&strcmpi(field1, 'mesh')) % plot mesh in double-entry mde
        varargin = {varargin{:}, 'mesh', 'on'};
        
        mydisp(150, ['launching ffpeplot with the following options :']);
        if (verbosity >= 150)
            varargin
        end;
         if(strcmpi(symmetry,'xm'))
        mesh.points(2,:) = -mesh.points(2,:);
        symmetry = 'no';
    elseif(strcmpi(symmetry,'ym'))
        mesh.points(1,:) = -mesh.points(1,:); 
        symmetry ='no';
         end  
  
        handle = ffpdeplot(mesh.points, mesh.bounds, mesh.tri, varargin{:});
        
    else
        % plot data (contour plot)
        % first prepare data 
        if(~iscell(field1))
       % case for contour plots
        if (~isnumeric(field1))
            % check if data to plot is the name of a field or a numerical dataset
            [~, field, suffix] = fileparts(field1); % to extract the suffix
            if (strcmp(suffix, '.im') == 1)
                data = imag(Ampplot*getfield(FFdata, field));
            else
                data = real(Ampplot*getfield(FFdata, field));
            end
        else
            data = field1;
        end
        varargin = {'xydata', data, varargin{:}};
        else
           % case for quiver plots
           data = real(Ampplot*getfield(FFdata, field1{1}));
           data2 = real(Ampplot*getfield(FFdata, field1{2}));
        varargin = {'flowdata', [data,data2], varargin{:}};   
        end
        
        if (strcmpi(contourval,'off')~=1&&strcmpi(contourval,'on')~=1)
        varargin{icontour} = 'on';
        [~, field, suffix] = fileparts(contourval); % to extract the suffix
            if (strcmp(suffix, '.im') == 1)
                xydata = imag(getfield(FFdata, field));
            else
                xydata = real(getfield(FFdata, field));
            end
        varargin = { varargin{:} , 'cxydata',xydata } ;
        end
        
         if (logscaleS~=-1)
            varargin = {varargin{:}, 'ColorRangeTicks', logscaleS};
            data = logfilter(data,logscaleS);
        end
        
        mydisp(20, ['launching ffpeplot with the following options :']);
        if (verbosity >= 20)
            varargin
        end;
          
 pointsS = FFdata.mesh.points;
 if(strcmpi(symmetry,'xm'))
        pointsS(2,:) = -pointsS(2,:);
        symmetry = 'no';
 elseif(strcmpi(symmetry,'ym'))
        pointsS(1,:) = -pointsS(1,:); 
        symmetry ='no';
 end
  

        if(length(data)==mesh.np)
            mydisp(10,'plotting P1 field');
            %handle = ffpdeplotP1(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri,varargin{:});% to be removed
            varargin={ varargin{:}, 'VhSeq', reshape(FFdata.mesh.tri(1:3,:),size(FFdata.mesh.tri,2)*3,1)-1};
            handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, varargin{:});
            
        elseif(length(data)==3*mesh.nt)
            mydisp(10,'plotting P1 field (from vectorized data)');
            %handle = ffpdeplotP1(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri,varargin{:});% to be removed
            varargin={ varargin{:}, 'VhSeq',  [0 : 3*mesh.nt-1]}; % [0 3*mesh.nt-1]
            handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, varargin{:});    
            
        elseif isfield(mesh,'np2')&&(length(data)==mesh.np2)
            mydisp(10,'plotting P2 field');
            varargin={ varargin{:}, 'VhSeq', mesh.Vh_P2};
            handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, varargin{:});
 
        elseif(length(data)==6*mesh.nt) 
            mydisp(10,'plotting P2 field (from vectorized data)');
            varargin={ varargin{:}, 'VhSeq', [0 : 6*mesh.nt-1]}; % replace by % [0 6*mesh.nt-1]
            handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, varargin{:});     
            
        elseif isfield(mesh,'np1b')&&(length(data)==mesh.np1b)
            mydisp(10,'plotting P1b field');
            varargin={ varargin{:}, 'VhSeq', mesh.Vh_P1b};
            handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, varargin{:});    
        
        elseif(length(data)==4*mesh.nt)
            mydisp(10,'plotting P1b field (from vectorized data)');
            varargin={ varargin{:}, 'VhSeq', mesh.P1bconnectivityVECT}; % replace by % [0 4*mesh.nt-1]
            handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, varargin{:});    
        else
            disp('np, np2 , nt, ndata');
            mesh.np
            mesh.np2
            mesh.nt
            length(data)
            error('Error : data size does not fit with P1, P2, P1b dimensions')
            
        end
 
 % handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, 'xydata', data, varargin{:});


%%% SYMMETRIZATION OF THE PLOT
if(strcmp(symmetry,'no'))
        mydisp(20,'No symmetry');
else   
     mydisp(20,['Symmetrizing the plot with option ',symmetry]);
  pointsS = FFdata.mesh.points;
  switch(symmetry)
    case('XS')
        pointsS(2,:) = -pointsS(2,:);dataS = data;
    case('XA')
       pointsS(2,:) = -pointsS(2,:);dataS = -data;
    case('YS')
        pointsS(1,:) = -pointsS(1,:);dataS = data;
    case('YA')
        pointsS(1,:) = -pointsS(1,:);dataS = -data;
    case({'XM','YM'})
          % do nothing as these case has already been treated
      otherwise
        error(' Error in plotFF with option symmetry ; value must be XS,XA,YS,YA,XM,YM or no')      
  end
  
    hold on;
    handle = ffpdeplot(pointsS, FFdata.mesh.bounds, FFdata.mesh.tri, varargin{:},'xydata', dataS);
    hold off;
end
    end
    
else % plot mesh only in single-entry mode
    % (not recommended any more but kept for legacy)
    varargin = {'mesh', varargin{:}}; 
    SF_Plot(FFdata,varargin{:});
    % old method
end

end




% custom colormaps
function cmap = redblue()
%colmapdef=[193,0,0; 235,164,164; 235,235,235; 196,196,255; 127,127,255]/255;
colmapdef=[127,127,255; 196,196,255; 235,235,235; 235,164,164; 193,0,0  ]/255;
[sz1,~]=size(colmapdef);
cmap=interp1(linspace(0,1,sz1),colmapdef,linspace(0,1,255));
end

function cmap = french()
colmapdef=[255,0,0; 255,255,255; 0,0,255]/255;
[sz1,~]=size(colmapdef);
cmap=interp1(linspace(0,1,sz1),colmapdef,linspace(0,1,255));
end

function cmap = ice()
%definition of the colormap "ice"
colmapdef=[255,255,255; 125,255,255; 0,123,255; 0,0,124; 0,0,0]/255;
[sz1,~]=size(colmapdef);
cmap=interp1(linspace(0,1,sz1),colmapdef,linspace(0,1,255));
end

function cmap = fire()
% definition of colormap "fire"
colmapdef = [255   255   255
             255   255   151
             255   207    89
             255   148    45
             255    99    13
             253    57     0];
         colmapdef = colmapdef/255;
[sz1,~]=size(colmapdef);
cmap=interp1(linspace(0,1,sz1),colmapdef,linspace(0,1,255));
end

function cmap = dawn()
% definition of colormap "dawn"
colmapdef = [255   255   195
   255   255   139
   255   179   126
   204    77   127
   101     0   127
     0     0   126];
     colmapdef = colmapdef/255;
[sz1,~]=size(colmapdef);
cmap=interp1(linspace(0,1,sz1),colmapdef,linspace(0,1,255));
end

function cmap = seashore()
% definition of colormap "seashore"
colmapdef = [[255   255   195];[255   255   139];[179   255   126];[77   204   127];[ 0   101   127];[0     0   126]]
colmapdef = colmapdef/255;
[sz1,~]=size(colmapdef);
cmap=interp1(linspace(0,1,sz1),colmapdef,linspace(0,1,255));
end

% Note for future (and for Javier) : here is the way to convert a [255,3]
% array into a [5,3] array producing an equivalent colormap
% colmapdef = colmapdef(1+255*(0:5)/5,:)
% colmapdef = colmapdef/255;


function y = logfilter(x,S)
y = S*sign(x).*log(1+abs(x)/S);
end
