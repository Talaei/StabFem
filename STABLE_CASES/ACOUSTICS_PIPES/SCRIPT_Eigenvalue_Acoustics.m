%% initialisation
%clear all
%close all
run('../../SOURCES_MATLAB/SF_Start.m');
verbosity=20;
%set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
%set(groot, 'defaultLegendInterpreter','latex');
r=1;
%% Mesh computation
ffmeshInit = SF_Mesh('Mesh_1.edp','Params',r);
%% Eigenvalue problem
% range = linspace(0.01,1.0,20);
% evListCM = [];
% evListS  = [];
% for lambda=range
%     [evCM,emCM] = SF_Stability(ffmeshInit,'shift',lambda,'BC','CM','nev',10);
%     [evS,emS] = SF_Stability(ffmeshInit,'shift',lambda,'BC','SOMMERFELD','nev',10);
%     evListCM = [evCM, evListCM];
%     evListS  = [evS,  evListS];
% end
%% Plot
% figure; SF_Plot(emCM(1),'p');
% figure; SF_Plot(emS(1),'p');
% 

[evCM,emCM] = SF_Stability(ffmeshInit,'shift',0.8,'BC','CM','nev',10);
[evS,emS] = SF_Stability(ffmeshInit,'shift',1,'BC','SOMMERFELD','nev',10);
evCMS = evCM.^2
figure;
plot(imag(evCMS),real(evCMS),'s');
hold on;
plot(imag(evCM),real(evCM),'x');
hold off;

