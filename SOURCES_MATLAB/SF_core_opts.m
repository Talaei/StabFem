function varargout = SF_core_opts(mode)
% Function reading the user option file if it exists in current path.
global sfopts

optsFileName = 'stabfem.opts';

switch mode
    case 'write'
        if isempty(sfopts)
            SF_core_log('e', 'SF_core_opts/write: Options are currently empty.');
            return;
        end
        optsFileName = [sfopts.sfroot '/SOURCES_MATLAB/' optsFileName];
        [s,fHdl] = SF_core_syscommand('fopen', optsFileName, 'w', 'l', 'UTF-8');
        if s==1
            SF_core_log('e', 'SF_core_opts/write: Could not open option file in writing mode.');
            return;
        end
        
        listOptsNames = fieldnames(sfopts);
        for i=1:numel(listOptsNames)
            varName = listOptsNames{i};
            varValue = sfopts.(varName);
            fprintf(fHdl,'%s',stringFromVariable(varName,varValue));
        end
        
        s = SF_core_syscommand('fclose', fHdl);
        if s==1
            SF_core_log('w', 'SF_core_opts/write: Option file could not be properly closed.');
            return;
        end
        
    case 'read'
        if exist(optsFileName, 'file')~=2
            SF_core_log('n', sprintf('SF_core_opts/read: %s could not be found', optsFileName));
            sfopts = [];
            return;
        end
        
        sfopts = struct();
        [s,fHdl] = SF_core_syscommand('fopen', optsFileName, 'r', 'l', 'UTF-8');
        if s==1
            SF_core_log('e', 'SF_core_opts/read: Could not open option file in reading mode.');
            return;
        end
        fline = fgets(fHdl);
        while ~isequal(fline,-1)
            opt = strsplit(fline(1:end-1),':'); %% use strplit instead of split ! (matlab 2015)
            if strcmp(opt{2},'char')
                sfopts.(opt{1}) = opt{4};
            else
                sfopts.(opt{1}) = cast(sscanf(opt{4},opt{3}),opt{2});
            end
            SF_core_log('d', sprintf('SF_core_opts/read: loaded option %s: %s',opt{1},opt{4}));
            fline = fgets(fHdl);
        end
        
        s = SF_core_syscommand('fclose', fHdl);
        if s==1
            SF_core_log('w', 'SF_core_opts/read: Option file could not be properly closed.');
            return;
        end
        
    case 'test'
        %TODO: test
        SF_core_log('dd', 'SF_core_opts/test: Starting testing current options');
        varargout{1} = true;
        SF_core_log('dd', 'SF_core_opts/test: Option test ended successfully (TODO: implement actual tests!).');

        
    otherwise
        SF_core_log('w', 'SF_core_opts: invalid option managing mode. Expected read, write or test.');
end

    function str=stringFromVariable(name,var)
        format = '';
        switch class(var)
            case 'char'
                format = 's';
            case 'logical'
                format = 'u';
            case 'uint8'
                format = 'u';
            case 'double'
                format = 'f';
            otherwise
                str=[];
                return;
        end
        str = sprintf(['%s:%s:%%%s:%' format '\n'],name,class(var),format,var);  
    end
end