%> @file SOURCES_MATLAB/SF_core_freefem.m
%> @brief Matlab function handling request to execute FreeFem++
%>
%> Usage: [status, msg] = SF_core_freefem(cmd, paramName, paramValue, ...)
%> @param[out] status: returned status code
%> @param[out] msg: output produced by system call
%>
%> Available parameters:
%>  * 'bin' (def: 'FreeFem++'): freefem executable name
%>  * 'parameters' (def: ''): parameters provided to FreeFem+++sf
%>  * 'arg' (def: ''): arguments to give to the freefem executable
%>  * 'errormsg' (def: ''): error message threw when problem detected
%>  * 'logfile' (def: ''): name of a file in which the freefem output
%>                         should be redirected
%>  * 'logpath' (def: ''): path of the directory in which create the
%>                         logfile
%>  * 'prepipe' (def: ''): command redirected through a pipe to executable
%>
%>
%> Resulting system command:
%> $ prepipe | ffdir/bin ffargs arg cmd
%> or
%> echo parameters | ffdir/bin sfopt.ffarg ffarg cmd
%> @author Maxime Pigou
%> @version 1.1
%> @date 12/11/2018 Start writing version 1.0
%> @date 20/11/2018 Switching to input parser treatment of arguments
function [status] = SF_core_freefem(cmd,varargin)
global sfopts
% Parse arguments
p = inputParser;
addParameter(p, 'bin', 'FreeFem++', @ischar);
addParameter(p, 'parameters', '', @ischar);
addParameter(p, 'prepipe', '', @ischar);
addParameter(p, 'arg', '', @ischar);
addParameter(p, 'errormsg', '', @ischar);
addParameter(p, 'logfile', '', @ischar);
addParameter(p, 'logpath', '', @ischar);
parse(p, varargin{:});

bin = p.Results.bin;

% we should provide either 'parameter' or 'prepipe'
if ~isempty(p.Results.parameters)
    prepipe = [ 'echo ' p.Results.parameters];
elseif ~isempty(p.Results.prepipe)
   prepipe = p.Results.prepipe;
else 
    prepipe = '';
end

errormsg = p.Results.errormsg;
logfile = p.Results.logfile;
logpath = p.Results.logpath;

% appends the options to the default ones according to verbosity level
if(sfopts.verbosity<10)
    args = [ sfopts.ffarg p.Results.arg ];
else
     args = [ sfopts.ffargDEBUG p.Results.arg ];
end

% Check options definition
if ~SF_core_opts('test')
    SF_core_log('e', 'SF_core_freefem: current options do not form a consistent execution environment.');
    status = 1;
    return;
end

% Check requested binary existence
ffbin = sprintf('%s/%s', sfopts.ffroot, bin);
if exist(ffbin,'file')~=2
    SF_core_log('e', 'SF_core_freefem: FreeFem++ binary does not exist.');
    status = 2;
    return;
end

% Check program (either in current directory or in ffdir common directory) 
if (exist(cmd,'file'))
    SF_core_log('n',[ 'SF_core_freefem : solver ' cmd '  found in current directoty']);
else
    if(exist([sfopts.ffdir cmd],'file'))
         SF_core_log('n',[ 'SF_core_freefem : solver ' cmd '  found ffdir common directoty']);
         cmd = [sfopts.ffdir cmd];
    else
        SF_core_log('e',[' Error in SF_Launch : FreeFem++ program ' ,cmd, ' not found']);
    end
end


if ~isempty(prepipe)
    ffcmd = sprintf('%s | %s %s %s %s', prepipe, ffbin, sfopts.ffarg, args, cmd);
else
    ffcmd = sprintf('%s %s %s %s', ffbin, sfopts.ffarg, args, cmd);
end



SF_core_log('nn', 'SF_core_freefem: starting freefem execution');
SF_core_log('nn', '===========================================');
SF_core_log('nn', '$$                                         ');
SF_core_log('nn', '$$                                         ');
SF_core_log('nn', sprintf('$$ > %s', ffcmd));
SF_core_log('nn', '$$                                         ');

if(sfopts.verbosity>=4)%&&sfopts.verbosity<10)
    [s] = system(ffcmd); % FreeFem outputs are displayed 'on the flight' 
else
    [s,t] = system(ffcmd); % FreeFem outputs are not displayed (but put in variable t) 
end
%if ~isempty(t); SF_core_log('dd', t); end % DAVID : no

SF_core_log('nn', '$$                                         ');
SF_core_log('nn', '===========================================');
SF_core_log('nn', 'SF_core_freefem: ending freefem execution');

if ~isempty(logfile)
    logRedirect = true;
    logFilePath = logfile;
    if ~isempty(logpath)
        if exist(logpath,'dir')~=7
            SF_core_log('w', 'SF_core_freefem: folder for log redirection does not exist, no redirection.');
            logRedirect = false;
            return;
        else
            logFilePath = SF_core_path(sprintf('%s/%s',logpath,logfile));
        end
    end
    if logRedirect
        fh = SF_core_file('fopentextwrite', logFilePath);
        fprintf(fh, t);
        fclose(fh);
    end
end


if s~=0 || (sfopts.isoctave && (s~=141 && s~=13))
    if ~isempty(errormsg)
        SF_core_log('e', sprintf('SF_core_freefem: %s', errormsg));
    end
    SF_core_log('d', 'SF_core_freefem: Error occured during FreeFem++ execution.');
    status = 10;
else
    status = 0;
end
end