#!/bin/bash
mkdir -p public/stable
mkdir -p public/development
cp -r STABLE_CASES/ACOUSTICS_PIPES/html/ 		public/stable/acoustic_pipes
cp -r STABLE_CASES/CYLINDER/html/ 			public/stable/cylinder
cp -r STABLE_CASES/EXAMPLE_Lshape/html/ 			public/stable/lshape
cp -r STABLE_CASES/WhistlingHole/html/ 			public/stable/whistling
cp -r STABLE_CASES/DNS_CYLINDER/html/ 			public/stable/cylinder_dns
cp -r STABLE_CASES/CompressibleCylinder/html/ 		public/stable/CompressibleCylinder
cp -r STABLE_CASES/LiquidBridges/html/ 			public/stable/LiquidBridges
cp -r STABLE_CASES/SLOSHING/html/ 			public/stable/SLOSHING
cp -r STABLE_CASES/ROTATING_POLYGONS/html/		public/stable/ROTATING_POLYGONS
cp -r DEVELOPMENT_CASES/BirdCallCOMPLEX/html/		public/development/BirdCallCOMPLEX
cp -r DEVELOPMENT_CASES/BLUNTBODY_IN_PIPE/html/ 	public/development/BLUNTBODY_IN_PIPE
cp -r DEVELOPMENT_CASES/Resonateur_Design1_Ailettes/ 	public/development/Resonateur_Design1_Ailettes
