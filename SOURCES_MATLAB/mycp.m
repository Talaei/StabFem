function mycp(file1, file2)
% This is a platform-independent tool to copy a file on a different name

% global sfopts
% if ~isempty(sfopts)
%     SF_core_log('d', 'USE OF LEGACY FUNCTION DETECTED:');
%     SF_core_log('d', sprintf('Please replace legacy command "mycp(''%s'',''%s'')"',file1,file2));
%     SF_core_log('d', sprintf('By new command "SF_core_syscommand(''cp'',''%s'',''%s'')"',file1,file2));
%     SF_core_syscommand('cp', file1, file2);
%     return;
% end

if(exist(file1)~=2)
    error(['Error in mycp : file ',file1,' does not exist !\n%s\n%s'],...
        ' If this error occurs during SF_BaseFlow, the file associated to your previous baseflow may have disapeared on a previous adapt',...
        ' Try typing ''SF_Status'' and following the recovery procedure...')
  
end

if (strcmp(file1, file2) == 0) % cp will only be called if filenames are different !
    
    if (isunix || ismac)
        command = ['cp ', file1, ' ', file2];
        system(command);
    end
    
    if (ispc)
        command = ['copy ', file1, ' ', file2];
        command = strrep(command, '/', '\');
        system(command);
    end
    
end
end
