%> @file SOURCES_MATLAB/SF_core_log.m
%> @brief Matlab function managing StabFem output messages.
%>
%> Usage: SF_core_log(level, message, enforce)
%> @param[in] level : message importance level ('d', 'n', 'w' or 'e')
%> @param[in] message : message string
%> @param[in] enforce : optional logical value, may enforce message display
%>
%> Message importance levels:
%>    'ddd' : dd + freefem debugging (high verbosity)
%>    'dd' : advanced debug messages
%>    'd'  : debug message, indicates actions that are performed by the code
%>    'nnn': nn + Legacy warning
%>    'nn' : advanded notice messages (notice + freefem outputs)
%>    'n'  : notice message, indicates progress to user (entering/leaving script and main results)
%>    'w'  : warning message, alerts the user on important events that do not
%>           prevent the execution continuation
%>    'e'  : error message, alerts the user on fatal events that prevent
%>           execution continuation
%>
%> Message are displayed if "enforce" is set to "true" or if user defined
%> verbosity level exceeds message importance level.
%>
%> User verbosity levels:
%>  0: no message shown
%>  1: error messages shown
%>  2: warning messages shown (+ error)
%>  3: notice messages shown (+ warning and error)
%>  4: double notice shown
%>  5: triple notice
%>  6: debug
%>  7: double debug
%>  8: triple debug
%>
%> @author Maxime Pigou
%> @version 1.1
%> @date 25/10/2018 Start writing version 1.0
%> @date 04/04/2019 Adding new verbosity levels with David. Version 1.1.
function SF_core_log(level, message, enforce)

if nargin<2 || nargin>3
    SF_core_log('w', 'SF_core_log misused: invalid number of arguments.', true);
    return;
end
if nargin==2
    enforce = false;
end
enforce = logical(enforce);

global sfopts
if isempty(sfopts) || ~isfield(sfopts,'verbosity')
    enforce = true;
else
    ulevel = sfopts.verbosity;
end

if enforce
    ulevel = 6;
end

if ~ischar(level)
    SF_core_log('w', 'SF_core_log misused: unexpected message level.', true);
    return;
end

level = lower(level(1));
switch level
    case 'ddd'
        fid = 1;
        nlevel = 8;
        prefix = 'DEBUG++ ';
    case 'dd'
        fid = 1;
        nlevel = 7;
        prefix = 'DEBUG+  ';
    case 'd'
        fid = 1;
        nlevel = 6;
        prefix = 'DEBUG   ';
    case 'nnn'
        fid = 1;
        nlevel = 5;
        prefix = 'NOTICE++';
    case 'nn'
        fid = 1;
        nlevel = 4;
        prefix = 'NOTICE+ ';
    case 'n'
        fid = 1;
        nlevel = 3;
        prefix = 'NOTICE  ';
    case 'w'
        fid = 2;
        nlevel = 2;
        prefix = 'WARNING ';
    case 'e'
        fid = 2;
        nlevel = 1;
        prefix = 'ERROR   ';
    otherwise
        SF_core_log('w', 'SF_core_log misused: unexpected mesage level.', true);
        return;
end

if ulevel>=nlevel
    if(ischar(message))
        fprintf(fid,'%s- %s\n',prefix,message);
    else
        disp(message)
    end
end

if(nlevel==1&&ulevel>0)
    error('stop here')
    %% David stop after an "error" except if sfopts.verbosity = 0
end

end