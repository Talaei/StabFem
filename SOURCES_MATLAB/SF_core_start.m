function SF_core_start(varargin)
% Function loading user options for StabFem and checking the correct
% definition of execution environment. The main role is to set the sfopts 
% global variable 
%
% usage :
%  [] = SF_core_start( ['mode',mode] [,'workdir',workdir] [,'verbosity',verbosity])
% % options :
%   'mode' , 'force' | 'normal' (default). Value 'force' will force to
%   reconstruct the sfopts global variable even if aleady exists
%   'workir' -> working directory (sfopts.ffdatadir) | default './WORK/'
%   'verbosity' -> verbosity level | default 4 ('nn')
%   (these two options are to be removed from here)
%
% This function must be executed once with the current path being located
% at the repository root.
%
%  force the system to repeat the installation, use SF_core_start('mode','force')
%
%  
% TO DO : read/write sfopts.ffdir, sfopts.arg, sfopts.argDEBUG in file
% ( but not sfopts.verbosity and sfopts.ffdatadir ? )
%
% printhelp() ?
%
% == Load user options for stabfem ==
% NB AT THIS STAGE inputParser may not be available ! use alternate method
    numvarargs = length(varargin);
    optsnames = {'mode','workdir','verbosity'};
    vararginval = {'auto','./WORK/',4};
    if (numvarargs>0)
        if (~mod(numvarargs,2))
            for i=1:2:(numvarargs-1)
                pos=find(strcmpi(optsnames,varargin(i)));
                if ~isempty(pos)
                    vararginval(pos)=varargin(i+1);
                else
                    %printhelp();
                    fprintf('%s\n',char(varargin(i)));
                    error('unknown input parameter');
                end
            end
        else
            %printhelp();
            error('wrong number arguments');
        end
    end
    [mode,workdir,verbosity] = vararginval{:};


global sfopts

if ~isempty(sfopts)&&~strcmpi(mode,'force');
    SF_core_log('n','Initialization already done. you may already use StabFem.')
    sfopts.verbosity=verbosity;
    SF_core_log('n',['Verbosity level set to ',num2str(verbosity)]);
    sfopts.ffdatadir = workdir;
    SF_core_log('n',['Working dir is ',workdir]);
    sfopts.ffarg = '-nw -v 0';
    sfopts.ffargDEBUG = '-v 10';
    sfopts.ffdir = [sfopts.sfroot '/SOURCES_FreeFem/']; 
    return;
end

% == Add Octave sources to path ==
%addpath('SOURCES_MATLAB');

% -- read options --
SF_core_opts('read');
%sfopts.ffargDEBUG = sfopts.ffarg; %% do this more cleanly in future

% -- ask user to generate new option file if required --
if isempty(sfopts)||(strcmpi(mode,'force')) 
    SF_core_generateopts();
    SF_core_opts('write');
end

% -- test the current environment --
SF_core_opts('test');

    
% -- A number of other needed variables
    sfopts.ffarg = '-nw -v 0';
    sfopts.ffargDEBUG = '-v 10';
    sfopts.ffdir = [sfopts.sfroot '/SOURCES_FreeFem/']; 
    
% -- TO BE DONE SOMEWHERE ELSE
    sfopts.verbosity=verbosity;
    SF_core_log('n',['Verbosity level set to ',num2str(verbosity)]);
    sfopts.ffdatadir = workdir;
    SF_core_log('n',['Working dir is ',workdir]);

% -- TODO
% should do something like this as well (or use freefem++.pref file)
%system('export FF_INCLUDEPATH="~/StabFem/SOURCES_FREEFEM" '); % or use SETENV
%system('export FF_LOADPATH="~/StabFem/SOURCES_OTHER"');
    

% -- Finalizing --

SF_core_log('n','Initialization ended. You may now use StabFem.');
end