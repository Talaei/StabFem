%% initialisation
clear all
%close all
run('../../SOURCES_MATLAB/SF_Start.m');
%set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
%set(groot, 'defaultLegendInterpreter','latex');
verbosity=20;
%rList = [0.1, 0.2, 0.5, 1.0, 2.0];
rList = [1.0];
IMPCMList = [];
IMPPMLList = [];
IMPSOMList = [];
for r=rList
    %% Chapter 1 : building an adapted mesh
    ffmeshInit = SF_Mesh('Mesh_1.edp','Params',r);
    Forced = SF_LinearForced(ffmeshInit,'omega',1,'BC','SOMMERFELD');
    figure();SF_Plot(Forced,'p');
    ffmesh = SF_Adapt(ffmeshInit,Forced,'Hmax',1); % Adaptation du maillage
    Forced2 = SF_LinearForced(ffmesh,'omega',4,'BC','CM');
    Forced1 = SF_LinearForced(ffmesh,'omega',0.1,'BC','CM');
    ffmesh = SF_Adapt(ffmesh,Forced1,Forced2,'Hmax',1); % Adaptation du maillage
    %% Computing Impedance for each considered radius and frequency
    IMP = SF_LinearForced(ffmesh,[0.01:1e-3:4.0],'BC','SOMMERFELD','plot','no')
    IMPCM = SF_LinearForced(ffmesh,[0.01:1e-3:4.0],'BC','CM','plot','no')
    IMPPML = SF_LinearForced(ffmesh,[0.01:1e-3:4.0],'BC','PML','plot','no')
    IMPCMList = [IMPCMList, IMPCM];
    IMPSOMList = [IMPSOMList, IMP];
    IMPPMLList = [IMPPMLList, IMPPML];
end

%% Compute error for each radius
% n = 6;
% 
% IMPErrorL1 = zeros(n);
% IMPErrorL2 = zeros(n);
% IMPErrorLinf = zeros(n);
% 
% IMPErrorRelL1 = zeros(n);
% IMPErrorRelL2 = zeros(n);
% IMPErrorRelLinf = zeros(n);
% 
% for k=[1:n]
%     IMPErrorLinf(k) = max(abs(IMPCMList(k).Z-IMPSOMList(k).Z));
%     IMPErrorRelLinf(k) = IMPErrorLinf(k)/max(abs(IMPCMList(k).Z));
%     IMPErrorL1(k) = sum(abs(IMPCMList(k).Z-IMPSOMList(k).Z));
%     IMPErrorRelL1(k) = IMPErrorL1(k)/sum(abs(IMPCMList(k).Z));
%     IMPErrorL2(k) = sum(abs(IMPCMList(k).Z-IMPSOMList(k).Z).^2)^0.5;
%     IMPErrorRelL2(k) = IMPErrorL2(k)/sum(abs(IMPCMList(k).Z).^2)^0.5;
% end

% plot(IMP.Z)
figure; plot(IMP.omega,IMP.Z, 'k--'); hold on;
        plot(IMP.omega,-IMPCM.Z, 'k-');
        plot(IMP.omega,IMPPML.Z, 'k-.');
