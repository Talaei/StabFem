#!/bin/bash

# =============================
# == Defining MATLAB command ==
# =============================
if [[ -z "${SF_AR_MATVER}" ]]; then
  export MATVER=r2018a
else
  export MATVER=${SF_AR_MATVER}
fi

MATPATH=/PRODCOM/bin/matlab${MATVER}

if [ ! -f "${MATPATH}" ]; then
  echo "Could not find matlab at ${MATPATH}. Exiting..."
  exit 1
fi



# ================================
# == Defining FreeFem++ command ==
# ================================
source /PRODCOM/bin/config.sh
module load Collection/Gnu-MPICH
module load freefem++/3.61-1-gcc-7.3.0-mpich-3.2.1

if [[ -z "${SF_AR_FFBASEDIR}" ]]; then
  export FFBASEDIR=${EBROOTFREEFEMPLUSPLUS}
else
  export FFBASEDIR=${SF_AR_FFBASEDIR}
fi

if [ ! -f "${FFBASEDIR}/bin/FreeFem++" ]; then
  echo "Could not find FreeFem++ executable. Exiting..."
  exit 1
fi

# ==============================
# == Choosing autorun version ==
# ==============================
# SFARVER=short -> Short autorun testing only basic functionnalities
# SFARVER=long  -> Long autorun testing advanced functionnalities and
#                  physical results for cases of reference
if [[ -z "${SF_AR_SFARVER}" ]]; then
  export SFARVER="short"
else
  export SFARVER=${SF_AR_SFARVER}
fi

# =========================
# == Exporting SFBASEDIR ==
# =========================

export SFBASEDIR="$(pwd)"

# =====================
# == Running autorun ==
# =====================

echo "Using MATLAB from: ${MATPATH}"
echo "Using FreeFem++ from: ${FFBASEDIR}"
echo "Running a ${SFARVER} autorun."

cmd="${MATPATH} -sd AUTORUN -nodesktop -nosplash -r autorun_core -logfile autorun.log"
echo "Running cmd: ${cmd}"
$cmd

exit $?

