
run('../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
verbosity=1;


bf = SF_Load('lastbaseflow');
bf.mesh.symmetry='N';
bf = SF_BaseFlow(bf,'Re',120,'type','NEW');
% we look for unstable eigenvalues for Re = 120
[ev,em] = SF_Stability(bf,'Nev', 10, 'shift',.3+5i, 'sym','N');

% we have found one ! we now do a DNS using as an initial condition the
% baseflow perturbed by it
%%
startfield = SF_Add(bf,em(1),'coefs',[1 0.01]);

dt = 0.0001; % time step of the DNS
Nit = 1000; % total nulmber of time steps
iout = 10; % generate a plottable file each iout step

[DNSstats,DNSfields] =SF_DNS(startfield,'Re',120,'Nit',Nit,'dt',dt,'iout',iout);




i=1;
h = figure;
filename = 'html/DNS_SquareConfined.gif';
SF_Plot(DNSfields(i),'ux','xlim',[-2 10],'ylim',[-3 3 ],'title',['t = ',num2str(i-1)*dt],'boundary','on')
set(gca,'nextplot','replacechildren');

%% Generate a movie

for i=1:Nit/iout
    SF_Plot(DNSfields(i),'ux','xlim',[-2 10],'ylim',[-3 3 ],'title',['t = ',num2str(i)],'boundary','on');
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
   
end

