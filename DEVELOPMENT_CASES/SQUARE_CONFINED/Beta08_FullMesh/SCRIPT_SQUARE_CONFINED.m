
% Example of using StabFem for flow around a confined square 
%
% This case uses a half-mesh.
%
% CURRENTLY DOES NOT WORK BECAUSE BASE-FLOW DOES NOT CONVERGE 
% (most likely due to steady symmetry-breaking bifurcation)



run('../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
verbosity=10; %raise to give mode details on what the program is doing !  

BETA = 0.8;Lmax = 30;
ffmesh = SF_Mesh('Mesh_SquareConfined.edp','Params',[BETA -5 Lmax],'problemtype','2D');
bf=SF_BaseFlow(ffmesh,'Re',1);

bf=SF_BaseFlow(bf,'Re',10);
%bf=SF_Adapt(bf,'Hmax',.25);
bf=SF_BaseFlow(bf,'Re',50);
bf=SF_BaseFlow(bf,'Re',70);
bf=SF_Adapt(bf);
%bf=SF_BaseFlow(bf,'Re',100);
%bf=SF_BaseFlow(bf,'Re',120);
bf=SF_BaseFlow(bf,'Re',150);
%bf=SF_Adapt(bf,'Hmax',.25);
%bf=SF_BaseFlow(bf,'Re',170);
%bf=SF_BaseFlow(bf,'Re',200);
%bf=SF_Adapt(bf,'Hmax',.25);
%bf=SF_Adapt(bf,'Hmax',.25);
%disp(' ');
% optional :
%disp('mesh adaptation to SENSITIVITY : ')
[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'nev',1,'type','S');
[bf,em]=SF_Adapt(bf,em,'Hmax',10);

% plot the base flow
SF_Plot(bf,'ux');
% or if you prefer tecplot:
exportFF_tecplot(bf,'bf_08_100.plt')

% compute and plot an eigenmode :
[ev,em] = SF_Stability(bf,'shift',3.69i,'nev',1,'type','D');
SF_Plot(em,'ux');
% or if you prefer tecplot:
exportFF_tecplot(em,'ModeU_08_200.plt')

% unsteady branch

Re_LIN = [170 : 5: 250];
bf=SF_BaseFlow(bf,'Re',170);
[ev,em] = SF_Stability(bf,'shift',-.02+3.69i,'nev',1,'type','D');

lambda_LIN=[];
    for Re = Re_LIN
        bf = SF_BaseFlow(bf,'Re',Re);
        [ev,em] = SF_Stability(bf,'nev',1,'shift','cont');
        lambda_LIN = [lambda_LIN ev];
    end    

figure(20);subplot(2,1,1);hold on;
plot(Re_LIN,real(lambda_LIN),'b+-');
plot(Re_LIN,0*Re_LIN,'k:');

figure(20);subplot(2,1,2);hold on;
plot(Re_LIN,imag(lambda_LIN),'b+-');


    


% branches in the vicinity of zero

Re_LIN = [170 : 5: 250];
bf=SF_BaseFlow(bf,'Re',170);
[ev,em] = SF_Stability(bf,'shift',-.02+0i,'nev',1,'type','D');

lambda_LIN=[];
    for Re = Re_LIN
        bf = SF_BaseFlow(bf,'Re',Re);
        [ev,em] = SF_Stability(bf,'nev',10,'shift',0);
        lambda_LIN = [lambda_LIN ev];
    end    

figure(20);subplot(2,1,1);hold on;
plot(Re_LIN,real(lambda_LIN(1,:)),'r+-');
plot(Re_LIN,real(lambda_LIN(2,:)),'g+-');
%plot(Re_LIN,real(lambda_LIN(3,:)),'m+-');

figure(20);subplot(2,1,2);hold on;
plot(Re_LIN,imag(lambda_LIN(1,:)),'r+-');
plot(Re_LIN,imag(lambda_LIN(2,:)),'g+-');
%plot(Re_LIN,imag(lambda_LIN(3,:)),'m+-');
pause(0.1);


% steady branch for Re<170

Re_LIN = [170 : -5: 100];
bf=SF_BaseFlow(bf,'Re',170);
[ev,em] = SF_Stability(bf,'shift',0.16+0i,'nev',1,'type','D');

lambda_LIN=[];
    for Re = Re_LIN
        bf = SF_BaseFlow(bf,'Re',Re);
        [ev,em] = SF_Stability(bf,'nev',1,'shift','cont');
        lambda_LIN = [lambda_LIN ev];
    end    

figure(20);subplot(2,1,1);hold on;
plot(Re_LIN,real(lambda_LIN(1,:)),'r+-');
plot(Re_LIN,0*Re_LIN,'k:');

figure(20);subplot(2,1,2);hold on;
plot(Re_LIN,imag(lambda_LIN(1,:)),'r+-');



% Mise en forme finale de la figure

figure(20);subplot(2,1,1);
title(['Aspect ratio : ',num2str(BETA)]);
xlabel('Re');ylabel('$\sigma$','Interpreter','latex');
box on; 
set(gca,'FontSize', 16);
figure(20);subplot(2,1,2);
xlabel('Re');ylabel('$\omega$','Interpreter','latex');
box on; 
set(gca,'FontSize', 16);
pause(0.1);
saveas(gca,'Square_08_stabbranches',figureformat);


