% Example of using StabFem for flow around a confined square 
%
% This example 


%% Initialization 
run('../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
verbosity=100;

%% Computation of a base flow and adapted mesh USING HALF MESH

BETA = 0.8;
Lmax = 20;
ffmeshHALF = SF_Mesh('Mesh_SquareConfined.edp','Params',[BETA -5 Lmax],'problemtype','2D');
bf=SF_BaseFlow(ffmeshHALF,'Re',1);


for Re = [10 50 70 100]
    bf=SF_BaseFlow(bf,'Re',Re);
end
bf = SF_Adapt(bf);
%bf = SF_Split(bf);

%% CONSTRUCTS A FULL MESH BY GLUING WITH MIRROR IMAGE

ffmesh = SF_Mirror(bf.mesh);
figure;SF_Plot(ffmesh,'xlim',[-1 5]);

%% Then we have to restart the generation of base flows increasing Re
% (you should not do any more mesh adaptation after this point !)

bfSym=SF_BaseFlow(ffmesh,'Re',1);
figure;SF_Plot(bfSym,'ux','xlim',[-2 4]);

for Re = [10 50 70 100]
    bfSym=SF_BaseFlow(bfSym,'Re',Re,'type','NEW');
end

%% Trace de la solution symetrique a Re = 100
figure;SF_Plot(bfSym,'ux','xlim',[-2 4]);

%% Recherche d'une solution asym?trique
[ev,em] = SF_Stability(bfSym,'shift',0.1,'nev',1,'sym','N'); % IMPORTANT DE METTRE L'OPTION SYM, N
%%
bfGuess = SF_Add(bfSym,em,'coefs',[ 1 0.5]);
bf = SF_BaseFlow(bfGuess,'Re',100,'type','NEW');
bf.Fy
figure;SF_Plot(bf,'ux','xlim',[-2 4]);

% bf = SF_Load('lastbaseflow');
% %% boucle sur [100-120]
% Retab1 = [100 :2 : 120];
% for i = 1:length(Retab1)
%    Re = Retab1(i)
%    bf = SF_BaseFlow(bf,'Re',Re,'type','NEW');
%    Fxtab1(i) = bf.Fx;
%    Fytab1(i) = bf.Fy
%    uy3tab1(i) = SF_ExtractData(bf,'uy',3,0)
%    ux3tab1(i) = SF_ExtractData(bf,'ux',3,0)
% end
% figure(20);
% subplot(2,1,1); plot(Retab1,Fytab1);
% title('Lift force');xlabel('Re');ylabel('Fy');
% subplot(2,1,2); plot(Retab1,Fxtab1);
% title('Drag force');xlabel('Re');ylabel('Fx');
% figure(21);
% subplot(2,1,1); plot(Retab1,uy3tab1);
% title('Vertical velocity u_y at (x,y) = (3,0)');xlabel('Re');ylabel('u_y');
% subplot(2,1,2); plot(Retab1,ux3tab1);
% title('Axial velocity u_y at (x,y) = (3,0)');xlabel('Re');ylabel('u_x');
% %% Trace de la solution symetrique a Re = 120
% figure;SF_Plot(bf,'ux','xlim',[-2 4]);
% pause(0.1);
% 
% %% Seconde boucle sur [100 - 85] 
% Retab2 = [100 :-.5:92  91.9:-.1:90.3 90.25:-.05:90   89:-2:85 ];
% bf = bfGuess;
% for i = 1:length(Retab2)
%    Re = Retab2(i);
%    bf = SF_BaseFlow(bf,'Re',Re,'type','NEW');
%    Fxtab2(i) = bf.Fx;
%    Fytab2(i) = bf.Fy;
%    uy3tab2(i) = SF_ExtractData(bf,'uy',3,0); 
%    ux3tab2(i) = SF_ExtractData(bf,'ux',3,0); 
% end
% figure(20);
% subplot(2,1,1); hold on; plot(Retab2,Fytab2);
% subplot(2,1,2); hold on; plot(Retab2,Fxtab2);
% figure(21);
% subplot(2,1,1); plot(Retab2,uy3tab2);
% subplot(2,1,2); plot(Retab2,ux3tab2);
% 
% 
% 
% 
% 
% 
