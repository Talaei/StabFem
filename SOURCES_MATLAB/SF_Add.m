function res = BF_Add(ffdata1,ffdata2,varargin)

%% Very basic driver to add two fields ; to be improved
%% Note : the present version works only with two fields ; to be improved

global ff ffdir ffdatadir sfdir verbosity

p = inputParser;
addParameter(p, 'Amp1', 1);
addParameter(p, 'Amp2',1e-3);
addParameter(p, 'Coefs',[]);
parse(p, varargin{:});

if(length(p.Results.Coefs)==2)
    Amp1 = p.Results.Coefs(1);
    Amp2 = p.Results.Coefs(2);
else
    Amp1 = p.Results.Amp1;
    Amp2 = p.Results.Amp2;
end

% reads first file
fid1 = fopen(ffdata1.filename);
size1 = fscanf(fid1,'%d',1);
if(strcmp(ffdata1.datastoragemode(1:2),'Re'))
    data1raw = fscanf(fid1,'%f',size1);
    data1 = data1raw(1:1:end);
else
    data1raw = fscanf(fid1,' (%f,%f)',2*size1);
    data1 = data1raw(1:2:end-1)+1i*data1raw(2:2:end);
end
end1 = fscanf(fid1,'%f');
fclose(fid1);

% reads second file
fid2 = fopen(ffdata2.filename);
size2 = fscanf(fid2,'%d',1);
if(strcmp(ffdata2.datastoragemode(1:2),'Re'))
    data2 = fscanf(fid2,'%f',size2);
else
    data2raw = fscanf(fid2,' (%f,%f)',2*size2);
    data2 = data2raw(1:2:end-1)+1i*data2raw(2:2:end);
end
end2 = fscanf(fid2,'%f');
fclose(fid2);




if(size1~=size2) 
    error('Error : the fields have different structures and cannot be added')
end

% Generation of the .txt file
    data3 = real(Amp1*data1+Amp2*data2);
    mydisp(2,'Creating field by adding two fields');
    res.filename = [ ffdatadir 'Addition.txt'];
    res.datatype= 'Addition';
    res.DataDescription = ['Addition of two fields from files ' ffdata1.filename ' and ' ffdata2.filename];
    res.datastoragemode = ffdata1.datastoragemode;
    fid3 = fopen(res.filename,'w');
    fprintf(fid3,'%d \n',size1);
    fprintf(fid3,' %f %f %f %f %f \n',data3);
    if(strcmp(ffdata1.datatype,'BaseFlow')||strcmp(ffdata1.datatype,'MeanFlow'))
    fprintf(fid3,' %f \n',end1);
    end
    fclose(fid3);
    mydisp(10,'SF_Add : successfully written .txt . file for sum of two fields')
    
res.mesh = ffdata1.mesh;   
np = res.mesh.np;
    
% Construction of the fields of res
% res.ux = ffdata1.ux+ffdata2.ux, etc... for each field of relevant dimension

% Part of the code to be improved... 

 fieldnames1 = fieldnames(ffdata1);
   for i = 1:length(fieldnames1)
       if isnumeric(ffdata1.(fieldnames1{i}))&&(length(ffdata1.(fieldnames1{i}))>1)
           if isfield(ffdata2,fieldnames1{i})&&(length(ffdata1.(fieldnames1{i}))==length(ffdata2.(fieldnames1{i})))
               mydisp(10,['SF_Add : detected a field with same dimension in both :',fieldnames1{i}]); 
               res.(fieldnames1{i}) = real(Amp1*ffdata1.(fieldnames1{i})+Amp2*ffdata2.(fieldnames1{i}));
           end
           if(isfield(ffdata2,[fieldnames1{i},'1']))
              % trick for retrocompatibiliby because in legacy cases the
              % field names in some files were called 'ux1' for 'ux',
              % etc...
              % to be removed in future
              res.(fieldnames1{i}) = real(Amp1*ffdata1.(fieldnames1{i})+Amp2*ffdata2.([fieldnames1{i},'1']));
           end
       end
   end

    
end



    
    
