function varargout = SF_Status(type);
%>
%> This function allows to generate a useful summary of data present in the
%> working directories.
%> This can be useful to reconstruct structures from available data files,
%> for instance if you did a 'clear all' but data files are still present... 
%>
%> USAGE :
%> 1. SF_Status('MESHES')
%>  -> check the content of folder ffdatadir/MESHES
%>      (meshes available and corresponding baseflows)
%>
%> 2. SF_Status('BASEFLOWS')
%>  -> check the content of folder ffdatadir/BASEFLOWS
%>      (baseflows compatible with the current mesh)
%>
%> 3. SF_Status('MEANFLOWS')
%>  -> check the content of folder ffdatadir/MEANFLOWS
%>      (baseflows compatible with the current mesh)
%>
%> 4. SF_Status('DNSFLOWS')
%>  -> check the content of folder ffdatadir/BASEFLOWS
%>      (baseflows compatible with the current mesh)
%>
%> 5. SF_Status()  [ or SF_Status('ALL') ]
%>  -> check all subfolders of ffdatadir
%>
%> 6. [LastMesh, LastAdaptedBaseFlow,LastComputedBaseFlow,LastComputedMeanFlow,LastDNS]=SF_Status('all')
%>  (-> this last method is only for internal usage by SF_Load)
%>
%> NB this functions uses nestedSortStruct and SortStruct taken from
%> mathworks (to sort the files by dates)
%>


global ff ffdir ffdatadir sfdir verbosity

if(nargin==0)
    type = 'all';
end
if(nargout==0)
    isdisp=1;
else
    isdisp=0;
end


if (strcmp(lower(type),'meshes') + strcmp(lower(type),'all') )
    mymydisp(isdisp,'#################################################################################');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,['.... CONTENT OF DIRECTORY ', ffdatadir, 'MESHES :']);
    mymydisp(isdisp,'     (list of meshes previously created/adapted ; couples of .msh/.ff2m files )');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,'Name                      | Date                 | Nv (nb of vertices)'); %| (Number of vertices) ');
    meshdir = dir([ffdatadir, 'MESHES/*.msh']);
    if(length(meshdir)>0)
        meshdir = nestedSortStruct(meshdir, 'datenum');
        for i = 1:length(meshdir)
            name = meshdir(i).name;
            date = meshdir(i).date;
            fid = fopen([ffdatadir, 'MESHES/', name], 'r');
            headerline = textscan(fid, '%f %f %f', 1, 'Delimiter', '\n');
            nv = headerline{1};
            nt = headerline{2};
            ns = headerline{3};
            mymydisp(isdisp,[name, blanks(25-length(name)), ' | ', date, blanks(20-length(date)), ' | ', num2str(nv)]);
        end
        LastMesh = [ffdatadir, 'MESHES/',name];
    else
        LastMesh = [];
    end
    
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,'     (list of base flows associated to newly computed meshes ; couples of .txt/.ff2m files )');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,'Name                      | Date                 | Size (bytes) '); %| (Number of vertices) ');
    meshdir = dir([ffdatadir, 'MESHES/*.txt']);
    if(length(meshdir)>0)
        meshdir = nestedSortStruct(meshdir, 'datenum');
        for i = 1:length(meshdir)
            name = meshdir(i).name;
            date = meshdir(i).date;
            mymydisp(isdisp,[name, blanks(25-length(name)), ' | ', date, blanks(20-length(date)), ' | ', num2str(meshdir(i).bytes)]);
        end
        LastAdaptedBaseFlow = [ffdatadir, 'MESHES/',name];
    else
        LastAdaptedBaseFlow = '';
    end  
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' REMINDER : PROCEDURE TO RECOVER A PREVIOUSLY COMPUTED MESH/BASEFLOW')
        mymydisp(isdisp,'    type succesfully the three following commands (to be adapted for your case...) :');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,'    ffmesh = SFcore_ImportMesh(''./WORK/MESHES/mesh_adapt_Re100.msh'')');
        mymydisp(isdisp,'    ffmesh.problemtype = ''2D'' ');
        mymydisp(isdisp,'    bf = SF_ImportData(ffmesh,''./WORK/MESHES/BaseFlow_adapt_Re100.ff2m'')');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
end

if(strcmp(lower(type),'baseflows')+strcmp(lower(type),'all'))
    mymydisp(isdisp,'#################################################################################');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,['.... CONTENT OF DIRECTORY ', ffdatadir, 'BASEFLOWS']);
    mymydisp(isdisp,'     (list of base flows compatible with current mesh ; couples of .txt/.ff2m files )');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,'Name                      | Date                 | Size      | Re ');
    meshdir = dir([ffdatadir, 'BASEFLOWS/*.txt']);
    
    if(length(meshdir)>0)
        meshdir = nestedSortStruct(meshdir, 'datenum');    
        for i = 1:length(meshdir)
            name = meshdir(i).name;
            date = meshdir(i).date;
                        %
            % NEW PROCEDURE TO READ THE metadata from the .ff2m file
            % (currently only "Re" is recognized)
            % 
            metadata = SFcore_ImportData([ffdatadir, 'BASEFLOWS/',name],'metadataonly');
            if isfield(metadata,'Re')
                Re = metadata.Re;
            else
                Re = -1;
            end
            mymydisp(isdisp,[name, blanks(25-length(name)), ' | ', date,...
            blanks(20-length(date)), ' | ', num2str(meshdir(i).bytes), '   | ', num2str(Re)]);
            %
            % END NEW PROCEDURE
            %
            %mymydisp(isdisp,[name, blanks(25-length(name)), ' | ', date, blanks(20-length(date)), ' | ', num2str(meshdir(i).bytes)]);
        end
        LastComputedBaseFlow = [ffdatadir, 'BASEFLOWS/',name];  
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' REMINDER : PROCEDURE TO RECOVER A PREVIOUSLY COMPUTED BASEFLOW')
        mymydisp(isdisp,'    type succesfully the three following commands (to be adapted for your case...) :');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,'    ffmesh = SFcore_ImportMesh(''./WORK/mesh.msh'')'); 
        mymydisp(isdisp,'    ffmesh.problemtype = ''2D'' (or something else depending on your case)');
        mymydisp(isdisp,'    bf = SFcore_ImportData(ffmesh,''./WORK/BASEFLOWS/BaseFlow_Re100.ff2m'')');
        mymydisp(isdisp,'    bf = SF_BaseFlow(bf)');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
    else
        LastComputedBaseFlow = LastAdaptedBaseFlow;
    end
    
end


if(strcmp(lower(type),'meanflows')+strcmp(lower(type),'all'))
    mymydisp(isdisp,'#################################################################################');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,['.... CONTENT OF DIRECTORY ', ffdatadir, 'MEANFLOWS']);
    mymydisp(isdisp,'     (list of meanflow/nonlinear HB components compatible with current mesh ; couples of .txt/.ff2m files )');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,'Name                      | Date                 | Size          | Re ');
    
    meshdir = dir([ffdatadir, 'MEANFLOWS/*.ff2m']);
    if(length(meshdir)>0)
        meshdir = nestedSortStruct(meshdir, 'datenum');
        
        
        for i = 1:length(meshdir)
            name = meshdir(i).name;
            date = meshdir(i).date;
            mymydisp(isdisp,[name, blanks(25-length(name)), ' | ', date, blanks(20-length(date)), ' | ', num2str(meshdir(i).bytes)]);
        end
        LastComputedMeanFlow = [ffdatadir, 'MESHES/',name];
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' REMINDER : PROCEDURE TO RECOVER A PREVIOUSLY COMPUTED MEANFLOW / HB1 ')
        mymydisp(isdisp,'    type succesfully the three following commands (to be adapted for your case and use a similar format for HB2...) :');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,'    ffmesh = SFcore_ImportMesh(''./WORK/MESHES/mesh_adapt_Re100.msh'')');
        mymydisp(isdisp,'    ffmesh.problemtype = ''2D'' ');
        mymydisp(isdisp,'    bf = SF_ImportData(ffmesh,''./WORK/MESHES/BaseFlow_adapt_Re100.ff2m'')');
        mymydisp(isdisp,'    mode = SFcore_ImportData(ffmesh,''./WORK/MEANFLOW/SelfConsistentMode_Re100.ff2m'')');
        mymydisp(isdisp,'    [bf,mode] = SF_HB1(bf,mode)');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
    else
        LastComputedMeanFlow = '';
    end
end

if(strcmp(lower(type),'dnsfield')+strcmp(lower(type),'all'))
    mymydisp(isdisp,'#################################################################################');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,['.... CONTENT OF DIRECTORY ', ffdatadir, 'DNSFIELDS']);
    mymydisp(isdisp,'     (list of DNS Snapshots compatible with current mesh ; couples of .txt/.ff2m files )');
    mymydisp(isdisp,' ');
    mymydisp(isdisp,'Name                      | Date                 | Size ');
    
    meshdir = dir([ffdatadir, 'DNSFIELDS/*.ff2m']);
    if(length(meshdir)>0)
        meshdir = nestedSortStruct(meshdir, 'datenum');
        
        
        for i = 1:length(meshdir)
            name = meshdir(i).name;
            date = meshdir(i).date;
            mymydisp(isdisp,[name, blanks(25-length(name)), ' | ', date, blanks(20-length(date)), ' | ', num2str(meshdir(i).bytes)]);
        end
        LastDNS = [ffdatadir, 'DNSFIELDS/',name]; 

        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' REMINDER : PROCEDURE TO RECOVER A PREVIOUSLY COMPUTED DNS FIELD ')
        mymydisp(isdisp,'    type succesfully the three following commands (to be adapted for your case ...) :');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,'    ffmesh = SFcore_ImportMesh(''./WORK/MESHES/mesh_adapt_Re100.msh'')');
        mymydisp(isdisp,'    ffmesh.problemtype = ''2D'' ');
        mymydisp(isdisp,'    dns = SFcore_ImportData(ffmesh,''./WORK/DNSFIELDS/DNS.ff2m'')');
        mymydisp(isdisp,' ');
        mymydisp(isdisp,' ');
    else
        LastDNS = '';
    end
end

mymydisp(isdisp,' ');
mymydisp(isdisp,'#################################################################################');
mymydisp(isdisp,' ');

if(nargout>0)
    varargout = {LastMesh, LastAdaptedBaseFlow,LastComputedBaseFlow,LastComputedMeanFlow,LastDNS};
end

end %function

function mymydisp(isdisp,string)
if(isdisp)
    disp(string);
end
end
    