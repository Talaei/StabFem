function [] = SF_core_createarborescence()
global sfopts;
ffdatadir = sfopts.ffdatadir;

    %In case not already present, we create the ffdatadir folder and a few useful subfolders:
    if ~exist(ffdatadir,'dir')
        SF_core_syscommand('mkdir',ffdatadir);
    end
    if ~exist([ffdatadir 'MESHES'],'dir')
        SF_core_syscommand('mkdir',[ffdatadir 'MESHES']);
    end
    if ~exist([ffdatadir 'BASEFLOWS'],'dir')
        SF_core_syscommand('mkdir',[ffdatadir 'BASEFLOWS']);
    end
    if ~exist([ffdatadir 'MEANFLOWS'],'dir')
        SF_core_syscommand('mkdir',[ffdatadir 'MEANFLOWS']);
    end
    if ~exist([ffdatadir 'DNSFIELDS'],'dir')
        SF_core_syscommand('mkdir',[ffdatadir 'DNSFIELDS']);
    end
    
    if(isunix)
        system(['echo "// File automatically created by StabFem" > SF_Geom.edp']);
    end
    
    % a file SF_Geom should be present, even if blank 
    % (this line is here for retrocompatibility,  not sure it is still userul...)
    

end
