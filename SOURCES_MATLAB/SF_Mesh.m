function mesh = SF_Mesh(meshfile, varargin)
% Matlab/FreeFem driver for generating a mesh using a FreeFem++ program
%
% usage in one-input mode : mesh = SF_Mesh('Mesh.edp')
%
% usage in two-input mode : baseflow = SF_Init('Mesh.edp','Params',[Param1 Param2 ...])
%   in this case Params is an array containing the parameters (reals) needed by the
%   freefem script ;  for instance Reynolds number, wavenumber, etc...
%
% 'Mesh.edp' must be a FreeFem script which generates a file "mesh.msh".
%  For simple cases this file will be the only needed.
%
%  For more elaborate cases available in the StabFem project, this function
%  will also read the following auxiliary files (if present) : 
%  * mesh.ff2m
%  * mesh_connectivity.ff2m
%  * mesh_surface.ff2m
%
% 
% This file belongs to the StabFem project freely disctributed under gnu licence.



p = inputParser;
addParameter(p, 'Params', NaN);
addParameter(p, 'problemtype','');
addParameter(p, 'symmetry','N');
parse(p, varargin{:})

global ff
global sfopts

% creates the arborescence if needed
%if ~exist(sfopts.ffdatadir,'dir')
%    SF_core_syscommand('mkdir',sfopts.ffdatadir);
%else
%    myrm([sfopts.ffdatadir, '*.txt ', sfopts.ffdatadir, '*.ff2m ', sfopts.ffdatadir, '*.msh ']);
%end

% launches the FreeFem program
if (isnan(p.Results.Params))
    command = [ff, ' ', meshfile];
    stringparam = '';
else
    stringparam = [];
    for pp = p.Results.Params;
        stringparam = [stringparam, num2str(pp), '  '];
    end
    command = ['echo   ', stringparam, '  | ', ff, ' ', meshfile];
end
error = 'ERROR : SF_Mesh not working ! \n Possible causes : \n 1/ your "ff" variable is not correctly installed (check SF_Start.m) ; \n 2/ Your Freefem++ script is bugged (try running it outside the Matlab driver) ';
%mysystem(command, error);

    value = SF_core_freefem(meshfile,'parameters',stringparam);

% Cleans the arborescence
%if exist([sfopts.ffdatadir, 'MESHES'],'dir')
%    SF_core_sycommand('rm',[ffdatadir, 'MESHES'])
%    SF_core_sycommand('mkdir',[ffdatadir, 'MESHES'])
%end

if exist([sfopts.ffdatadir, 'BASEFLOWS'],'dir')
    SF_core_syscommand('rm',[sfopts.ffdatadir, 'BASEFLOWS']);
    SF_core_syscommand('mkdir',[sfopts.ffdatadir, 'BASEFLOWS']);
end

if exist([sfopts.ffdatadir, 'MEANFLOWS'],'dir')
    SF_core_syscommand('rm',[sfopts.ffdatadir, 'MEANFLOWS']);
    SF_core_syscommand('mkdir',[sfopts.ffdatadir, 'MEANFLOWS']);
end

%if exist([sfopts.ffdatadir, 'MESHES'],'dir')
%    myrm([ffdatadir, 'MESHES/*']);
%myrm([ffdatadir, 'BASEFLOWS/*']); this is done at the end

% Imports the mesh
%mesh = SFcore_ImportMesh([ffdatadir, 'mesh.msh']);

if ~strcmp(sfopts.ffdatadir,'./')
    meshfilename = SFcore_MoveDataFiles('mesh.msh','mesh_Init.msh','MESHES');
else
    meshfilename = 'mesh.msh';
end
    
mesh = SFcore_ImportMesh(meshfilename);



% sets keyword 'problemtype' and writes corresponding file 
if ~isempty(p.Results.problemtype)
    mesh.problemtype = p.Results.problemtype;
    SFcore_Writeff2mFile([sfopts.ffdatadir, 'problemtype.ff2m'],...
        'filedescription','This file was created by SF_Mesh','problemtype',mesh.problemtype);
    if(exist([sfopts.ffdatadir, 'MESHES'],'dir'))
        SFcore_Writeff2mFile([sfopts.ffdatadir, 'MESHES/problemtype.ff2m'],...
        'filedescription','This file was created by SF_Mesh','problemtype',mesh.problemtype);
    end
end

%sets keyword 'symmetry'
sym = p.Results.symmetry;
if (ismember(6,mesh.labels)&&abs(min(mesh.points(2,:))<1e-10)&&strcmpi(sym,'n'))
    sym = 'S';
end
mesh.symmetry=sym;

    
% Copies the files under aliases in the relevant directories

%if ~exist([sfopts.ffdatadir, 'MESHES'],'dir')
%  mymake( [sfopts.ffdatadir, 'MESHES']);
%end

if(exist([sfopts.ffdatadir, 'SF_Init.ff2m'],'file'))
   mycp([sfopts.ffdatadir, 'SF_Init.ff2m'],[sfopts.ffdatadir, 'MESHES/SF_Init.ff2m']);
end

if(exist([sfopts.ffdatadir, 'BASEFLOWS'],'dir')==7)
     SF_core_syscommand('rm',[sfopts.ffdatadir '/BASEFLOWS']);
     SF_core_syscommand('mkdir',[sfopts.ffdatadir '/BASEFLOWS']);
end

mydisp(1, ['      ### INITIAL MESH CREATED WITH np = ', num2str(mesh.np), ' points']);
