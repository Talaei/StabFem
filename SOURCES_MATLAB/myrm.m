function myrm(file)
% This is a platform-independent tool to delete a file

% global sfopts
% if ~isempty(sfopts)
%     SF_core_log('d', 'USE OF LEGACY FUNCTION DETECTED:');
%     SF_core_log('d', sprintf('Please replace legacy command "myrm(''%s'')"',file));
%     SF_core_log('d', sprintf('By new command "SF_core_syscommand(''rm'',''%s'')"',file));
%     SF_core_syscommand('rm', file);
%     return;
% end

if(exist(file)||length(strfind(file,'*'))>0)
	if (isunix || ismac)
	    command = ['rm ', file];
	    system(command);
	end

	if (ispc)
	    c1 = ['del /q '];
	    c2 = [file];
	    c2 = strrep(c2, '/', '\');
	    command = [c1, c2];
	    system(command);
	end
else
    mydisp(10,['Warning in myrm : file ',file,'does not exist'])  
end

end