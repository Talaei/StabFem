
run('../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
verbosity=1;


bf = SF_Load('lastbaseflow');


bf = SF_BaseFlow(bf,'Re',120,'type','NEW');
% we look for unstable eigenvalues for Re = 120
[ev,em] = SF_Stability(bf,'Nev', 10, 'shift',.3+5i, 'sym','N');

% we have found one ! we now do a loop to plot it
[ev,em] = SF_Stability(bf,'Nev', 1, 'shift',ev(1), 'sym','N'); % this line is useful to initialize the 'shift=cont' mode
Retab = 120:-2:90;
for i = 1:length(Retab)
    Re = Retab(i)
    bf = SF_BaseFlow(bf,'Re',120,'type','NEW');
    [ev,em] = SF_Stability(bf,'Nev', 1, 'shift','cont','sym','N');
    evtab(i) = ev
end
figure;
subplot(2,1,1);
plot(Retab,real(evtab));
label('\lambda_r');xlabel('Re');
subplot(2,1,2);
plot(Retab,imag(evtab));
label('\lambda_i');xlabel('Re');

