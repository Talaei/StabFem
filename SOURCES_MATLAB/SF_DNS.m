function [DNSstats,DNSfields] = SF_DNS(varargin)
%>
%> This is part of StabFem Project, D. Fabre, July 2017 -- present
%> Matlab driver for DNS
%>
%> USAGE :
%>    [DNSstats,DNSfields] = SF_DNS(DNS_Start,'Re',Re,'dt',dt,'itmax',itmax,[...])
%>    
%>  INPUT PARAMETERS : 
%>      DNS_Start is either a baseflow/meanflow or a previous DNS result
%>      (if an array of fields is probided, the last instant will be taken)
%>
%> OPTIONAL PARAMETERS :
%>
%> RESULTS :
%>    DNSstats : arrays containing time statistics 
%>               (history of lift,drag, or other customisable statistics)
%>
%>    DnsFields : array(N) of fields produced each iout time steps
%>
%> NB improved mode to compute meanflow by timeaveraging in three-output mode
%> [DNSstats,DNSfields,meanflow] = SF_DNS(....)
%> To immplement ? Diogo ??

global ff ffdir ffdatadir sfdir verbosity

startfield = varargin{1}(end);
ffmesh = startfield.mesh;
vararginopt = {varargin{2:end}};

p = inputParser;
addParameter(p, 'Re', 100);
addParameter(p, 'Ma', 0.2);
addParameter(p, 'rep',0);
addParameter(p, 'itmax', 0); % max step number
addParameter(p, 'Nit',1000 ); %  number of step
% NB you should provide either itmax or Nit but not both !
addParameter(p, 'dt', 5e-3);
addParameter(p, 'iout', 10);
addParameter(p, 'istat', 1);
addParameter(p, 'mode', 'init');
%addParameter(p, 'startmode',[]);
%addParameter(p, 'amplitudemode',1e-3,@isnum);
addParameter(p, 'dir',[ffdatadir 'DNSFIELDS'])  
addParameter(p,'PreviousStats','');
addParameter(p,'Solver','');
parse(p, vararginopt{:});

%startfield.datatype

switch(startfield.datatype)
    case {'BaseFlow','Meanflow','Addition'} 
        rep = 0;
        if strcmpi(p.Results.mode,'init')
            myrm([ffdatadir,'dns_Stats.ff2m']);
        end
        mydisp(1, ['FUNCTION SF_DNS : starting from BF / MF (reset it = 0)']);
            mycp(startfield.filename, [ffdatadir, 'dnsfield_start.txt']);
             
    case 'DNSField'
        rep = startfield.it;
        mydisp(1, ['FUNCTION SF_DNS : starting from previous DNS result with it = ', num2str(rep)]);
end

if ~(exist([ffdatadir 'DNSFIELDS'],'dir'))
    mymake([ffdatadir 'DNSFIELDS']);
end

    
if(p.Results.itmax==0)  
    itmax = rep+p.Results.Nit;
else
    itmax = p.Results.itmax;
end
mydisp(1, ['         : Time-stepping up to it = ',num2str(itmax) ' ( number of steps = ' num2str(itmax-rep) ' ) ']);
iout = p.Results.iout;


mydisp(1, ['FUNCTION SF_DNS : starting from step ',num2str(p.Results.rep)]);
mycp(startfield.mesh.filename, [ffdatadir, 'mesh.msh']);
%optionstringComp = [' ', num2str(p.Results.Re), ' ', num2str(p.Results.Ma), ' ', num2str(rep), ' ',num2str(itmax), ' ',num2str(p.Results.dt), ' ',num2str(p.Results.iout), ' ', num2str(p.Results.istat)];
%optionstringIncomp = [' ', num2str(p.Results.Re), ' ', num2str(rep), ' ',num2str(itmax), ' ',num2str(p.Results.dt), ' ',num2str(p.Results.iout), ' ', num2str(p.Results.istat)];

% launch ff++ code

switch (lower(startfield.mesh.problemtype))

    case('2d')
        FFsolver = 'TimeStepper_2D.edp'; % default
        optionstring = [' ', num2str(p.Results.Re), ' ', num2str(rep), ' ',num2str(itmax), ' ',num2str(p.Results.dt), ' ',num2str(p.Results.iout), ' ', num2str(p.Results.istat)];
    
    case('2dold')
        FFsolver = 'TimeStepper_2D_Convect.edp'
        optionstring = [' ', num2str(p.Results.Re), ' ', num2str(rep), ' ',num2str(itmax), ' ',num2str(p.Results.dt), ' ',num2str(p.Results.iout), ' ', num2str(p.Results.istat)]; 

  % case("your case...")
        % add your case here !
        
    otherwise
        error(['Error in SF_HB2 : your case ', meanflow.mesh.problemtype 'is not yet implemented....'])
        
    end

    if ~isempty(p.Results.Solver)
        FFsolver = p.Results.Solver;
        mydisp(1,'SF_DNS : Using alternative solver ');
    end
    command = ['echo ', optionstring, ' | ', ff, ' ',ffdir,FFsolver];

if(~strcmp(p.Results.mode,'postprocessonly'))
    mydisp(2,'Launching DNS...');
    errormessage = 'ERROR : TimeStepper aborted';
    status = mysystem(command, errormessage);
else
     mydisp(2,'import of a previous dataset from DNS...');
end

%%% GENERATE RESULTS : an array of "DNSflow" structures each iout steps

if((itmax-rep)/iout >= 1)
    for i=1:(itmax-rep)/iout
        dnsfilename = SFcore_MoveDataFiles(['dnsfield_',num2str(rep+i*iout),'.ff2m'],...
                   ['dnsfield_',num2str(rep+i*iout),'.ff2m'],[ffdatadir 'DNSFIELDS']);
         if (exist(dnsfilename,'file'))
            DNSfields(i) = SFcore_ImportData(ffmesh,dnsfilename);
         else
             mydisp(10,'WARNING : DNS HAS DIVERGED ')
             break
         end
    end
else
    DNSfields = [];
end
    
    DNSstats = SFcore_ImportData([ffdatadir,'dns_Stats.ff2m']);
    
    if ~isempty(p.Results.PreviousStats)
        mydisp(20,'Gluing stats with previous data')
        DNSstats = SFcore_MergeStructures(p.Results.PreviousStats,DNSstats);
    end
    
    if length(varargin{1})>1
        mydisp(20,'Gluing DNSfields with previous snapshots')
        DNSfields = [varargin{1} DNSfields];        
end