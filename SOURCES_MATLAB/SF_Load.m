function flow = SF_Load(mode)
%
% function SF_Load
% 
% This function is used to load previously computed meshes/baseflows ;
% useful when restarting from previously computed data
%
% USAGE : 
%   1/ mesh = SF_Load('lastmesh')
%       -> load mesh only
% 
%   2/ bf = SF_Load('lastadaptedbaseflow')
%    
%   3/ bf = SF_Load('lastbaseflow')
%
%   4/ mf = SF_Load('lastmeanflow') ( NOT YET VALIDATED !)
%
%   5/ dnsflow = SF_Load('lastDNS')
%
% This program belongs to the StabFem project, freely distributed under GNU licence.
% copyright D. Fabre, 31 march 2019

 

[LastMesh, LastAdaptedBaseFlow,LastComputedBaseFlow,LastComputedMeanFlow,LastDNS]=SF_Status('all');
if ~isempty(LastMesh)
    ffmesh = SFcore_ImportMesh(LastMesh);
    %sets keyword 'symmetry'
        if (ismember(6,ffmesh.labels)&&abs(min(ffmesh.points(2,:))<1e-10))
            sym = 'S';
            mydisp(20,'WARNING IN SF_Load : assuming symmetry = ''S'' ');
        else
            sym = 'N';
            mydisp(20,'WARNING IN SF_Load : assuming symmetry = ''S'' ');
        end
        ffmesh.symmetry=sym;

    switch lower(mode)
    case('lastmesh')
        flow = ffmesh;
    case('lastadaptedbaseflow')
        flow = SFcore_ImportData(ffmesh,LastAdaptedBaseFlow);
    case('lastbaseflow')
        if(exist(LastComputedBaseFlow,'file'))
            flow = SFcore_ImportData(ffmesh,LastComputedBaseFlow);
            SF_core_log('n',['Importing last base flow :' LastComputedBaseFlow]);
        else
            flow = [];
            SF_core_log('n','Last base flow not found');
        end
    case('lastmeanflow')% not validated
        flow = SFcore_ImportData(ffmesh,LastComputedMeanFlow);
    case('lastdns')
        flow = SFcore_ImportData(ffmesh,LastDNS);
    otherwise
        error('Error in SF_Load : option not recognized');
    end
    
else
    flow = [];
end
    
    
end
