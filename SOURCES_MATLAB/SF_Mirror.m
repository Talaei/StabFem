function ffmesh = SF_Mirror(halfmesh)
%
% transforms a half mesh into a full mesh
%
%
% This is part of the StabFem project, copyright D. Fabre, jkuly 2018.
%
% NB "backups in case of failure" are probably not useful anymore thanks
% to the new method to position entry files. To be checked and probably
% removed...
%
global ff ffdir ffdatadir sfdir verbosity

 mydisp(1, 'FUNCTION SF_Mirror : mirroring mesh for single mesh');
 mycp(halfmesh.filename, [ffdatadir, 'mesh.msh']);

% launch ff++ code
command = [ ff, ' ', ffdir, 'MirrorMesh.edp'];
errormessage = 'ERROR : FreeFem splitmesh aborted';
status = mysystem(command, errormessage);


meshfilename = SFcore_MoveDataFiles('mesh_mirror.msh',['mesh_mirror.msh'],[ffdatadir 'MESHES']);
ffmesh = SFcore_ImportMesh(meshfilename,'problemtype',halfmesh.problemtype);

%ffmesh = SFcore_ImportMesh('mesh_mirror.msh');
%ffmesh.problemtype = halfmesh.problemtype;

ffmesh.symmetry = 'N';

% info
mydisp(2, '      ### MIRROR MESH : ');
mydisp(2, ['      #   Number of points np = ', num2str(ffmesh.np)] ); 


myrm([ffdatadir, 'BaseFlow_guess.txt']);
myrm([ffdatadir, 'BASEFLOWS/*']);
    

end