
global sfopts; 
if ~exist('verbosity','var')
    verbosity = 3;
end
if ~exist('ffdatadir','var')
    ffdatadir = './WORK/';
end
    
SF_core_start();
sfopts.verbosity=verbosity;
sfopts.ffdatadir=ffdatadir;



if ~strcmp(ffdatadir,'./')
    disp('creating arborescence');
    SF_core_createarborescence;
end


% For LEGACY USE (to be removed soon !)
% global ff ffMPI ffdir ffdatadir sfdir verbosity
%ff = 'FreeFem++';
%ffMPI = 'FreeFem++-mpi';
%ffdir = sfopts.ffdir;
%sfdir = sfopts.sfdir;
 
 % 
%     disp('Entering SF_Start.'); % at this stage SF_core_log does not exist !
% %    %% New method
% 
%     SF_core_start();
%     
%     
%     sfopts.ffarg = '-v 0 -nw' ; % this should be done inside the SF_core_start
%     sfopts.ffargDEBUG = ' -v 1000 '; % idem
%     
%     % Using opts to fill expected global variables (LEGACY)
%     ff = SF_core_path([sfopts.ffroot '/bin/FreeFem++ ' ]);
%     ffMPI = SF_core_path([sfopts.ffroot '/bin/FreeFem++-mpi ']);
%     sfdir = SF_core_path([sfopts.sfroot '/SOURCES_MATLAB/']);
%     ffdir = SF_core_path([sfopts.sfroot '/SOURCES_FREEFEM/']);
% 
%     %    addpath(sfdir); -> now done in SF_core_opts
%        
%    if isempty(verbosity)
%         verbosity = 3; 
%         sfopts.verbosity = 3;
%     else
%         sfopts.verbosity = verbosity;
%    end
%     
%     if isempty(ffdatadir)
%         ffdatadir = './WORK/';
%         sfopts.ffdatadir = './WORK/';
%     else
%         sfopts.ffdatadir = ffdatadir;
%     end
%        
%     
%     
%     if ~strcmp(sfopts.ffdatadir,'./')&&~strcmp(sfopts.ffdatadir,'')
%         SF_core_createarborescence;
%     end
%     
%     
%     if(1==0)
% %else
% %% Old method
%     
% %> THE ROLE OF THIS SCRIPT IS TO POSITION THE following global variables :
% %> ff -> adress with full path of the FreeFem++ executable
% %> ffdir -> path of the FreeFem sources of the project 
% %>           (recommened is ~/StabFem/SOURCES_FREEFEM)
% %> sfdir -> path of the OCTAVE/Matlab sources of the project
% %>           (recommened is ~/StabFem/SOURCES_MATLAB)
% %> ffdatadir -> path where to store the results 
% %>           ( recommended is ./WORK)
% %> verbosity -> global variable controlling the volume of messages produced by the programs
% %>           ( recommended : 1 in stable mode ; 100 in debug mode...)
% 
% verbosity = 1;
% 
% 
% %% Adress of the executable :
% if (isunix)
%     %ff = '/usr/local/bin/FreeFem++ -v 0'; % on most systems
%     if ~isempty(getenv('EBROOTFREEFEMPLUSPLUS'))
%         ffroot = getenv('EBROOTFREEFEMPLUSPLUS');
%     elseif ~isempty(getenv('FFROOT'))
%         ffroot = getenv('FFROOT');
%     elseif exist('/PRODCOM/Ubuntu16.04/freefem/3.55/gcc-5.4-mpich_3.2/','dir')
%         ffroot = '/PRODCOM/Ubuntu16.04/freefem/3.55/gcc-5.4-mpich_3.2/';
%         warning('Legacy installation of Freefem++ was found. Please set FFROOT environment variable instead.');
%     elseif exist('/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++','file') % For David's mac
%         ffroot = '/usr/local/ff++/openmpi-2.1/3.61-1';
%         warning('Legacy installation of Freefem++ was found. Please set FFROOT environment variable instead.');
%     elseif exist('/usr/local/bin/FreFem++','file')
%         ffroot = '/usr/local';
%         warning('Legacy installation of Freefem++ was found. Please set FFROOT environment variable instead.');
%     else
%         error('Freefem++ root was not found. Set env. variable FFROOT.');
%     end
%     ff = [ffroot '/bin/FreeFem++ -v 0']; 
%     ffMPI = [ffroot '/bin/ff-mpirun -v 0'];
% end
% %if (ismac)
%     %ff = '/usr/local/bin/FreeFem++ -v 0  -glut /usr/local/bin/ffglut';
%  %   ff = '/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++ -v 0  -glut /usr/local/bin/ffglut';
%     %    ff = '/usr/local/bin/FreeFem++ -v 0'; % to use without ffglut (no graphics but more fluid)
%   %  ffMPI = '/usr/local/ff++/openmpi-2.1/3.60/bin/FreeFem++-mpi -v 0';
%   %ffMPI = '/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++-mpi -v 0';
% %    ffMPI = '/usr/local/bin/FreeFem++-mpi -v 0';    
%     % NB normally this is where the FreeFem++ executable should be on a mac.
%     % If not the case, either do a symbolic link (recommended) or replace with
%     % the right one. option "-nw" is better to discard the ff++ graphical output.
%     % below are possible choices for various contributors :
%     %ff = '/usr/local/ff++/openmpi-2.1/3.60/bin/FreeFem++'; % old syntax for David
%     %ff = '/usr/local/ff++/bin/FreeFem++'; for Flavio
% %end
% if (ispc)
%     ff = 'FreeFem++ -nw -v 0 -nw'; % for windows systems
%     ffMPI = 'ffm-mpirun -v 0 -nw';
% end
% 
% %% Directories where to find the programs :
% sfroot = [fileparts(mfilename('fullpath')) '/../'];
% 
% sfdir = [sfroot 'SOURCES_MATLAB/'];
% ffdir = [sfroot 'SOURCES_FREEFEM/'];
% % This is the recommended implementation on most systems.
% 
% % should do something like this as well (or use freefem++.pref file)
% %system('export FF_INCLUDEPATH="~/StabFem/SOURCES_FREEFEM" ');
% %system('export FF_LOADPATH="~/StabFem/SOURCES_OTHER"');
% 
% 
% % In case StabFem is not in your root directory you may adapt. Bellow a few
% % examples from various contributors.
% 
% %sfdir = '/Users/flavio/StabFem/SOURCES_MATLAB/'; % where to find the matlab drivers
% %ffdir = '/Users/flavio/StabFem/SOURCES_FREEFEM/'; % where to find the freefem scripts
% 
% %sfdir = '/Users/fabred/StabFem/SOURCES_MATLAB/'; % where to find the matlab drivers
% %ffdir = '/Users/fabred/StabFem/SOURCES_FREEFEM/'; % where to find the freefem scripts
% 
% %sfdir = '/home/jsagg/Sources/StabFem/SOURCES_MATLAB/';
% %ffdir = '/home/jsagg/Sources/StabFem/SOURCES_FREEFEM/';
% 
% addpath(sfdir);
% 
% %% Directory where to put working files
% %ffdatadir = './WORK/';
%     if isempty(ffdatadir)
%         ffdatadir = './WORK/';
%         sfopts.ffdatadir = './WORK/';
%     else
%         sfopts.ffdatadir = ffdatadir;
%     end
% 
%     % putting them here ; maybe there is a better way ?
%     sfopts.ffdir = [sfopts.sfroot '/SOURCES_FreeFem/']; 
%     sfopts.ffarg = '-nw -v 0'; 
%     sfopts.ffargDEBUG = '-v 10';
%     
% 
%  SF_core_createarborescence;
% 
%  % this is for transition to new method... 
%  SF_core_start();
%  sfopts.verbosity=0;
% 
% end