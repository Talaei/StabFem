function [wnl, meanflow, selfconsistentmode, secondharmonicmode] = SF_WNL(baseflow, eigenmode, varargin)
%
% Matlab/FreeFem driver for computation of weakly nonlinear expansion
% This is part of the StabFem project, version 2.1, july 2017, D. Fabre
%
% ONE-OUTPUT usage : wnl = SF_WNL(baseflow , eigenmode, {'param','value'} );
%    output is a structure with fields wnl.lambda, wnl.mu, wnl.a, etc..
%    'baseflow' should be a base-flow structure corresponding to the critical Reynolds
%     number. 'eigenmode' should be the corresponding neutral mode.
%
%  the optional parameters (couple of 'param' and 'value') may comprise :
%   'Normalization'  -> can be 'L' (lift), 'E' (energy of perturbation),
%                       'V' (velocity a one point), or 'none' (no normalization). Default is 'L'.
%   'AdjointType',   -> can be 'dA' for discrete adjoint or 'cA' for continuous adjoint (default 'dA')
%   'Retest'         -> Value of Reynolds number to generate a "guess" for the
%                     SC-HB methods (useful in three-output and four-output usage, see below)
%
% THREE-OUTPUT USAGE : [wnl,meanflow,selconsistentmode] = SF_WNL(baseflow,eigenmode,[option list])
% this will create an estimation of the meanflow and quasilinear mode, for
% instance to initiate the Self-Consistent model. Ideally the value of Retest
% should be slightly above the threshold.
%
% FOUR-OUTPUT USAGE : [wnl,meanflow,selconsistentmode,secondharmonicmode] = SF_WNL(baseflow,eigenmode,[option list])
%
% IMPLEMENTATION :
% according to parameters this generic driver will launch one of the
% following FreeFem programs :
%      WeaklyNonLinear_2D.edp
%      WeaklyNonLinear_Axi.edp
%      ( WeaklyNonLinear_BirdCall.edp : version to be abandonned)
%
% TODO :
%


global sfopts;
ff = 'FreeFem++';
ffdir = sfopts.ffdir;
ffdatadir = sfopts.ffdatadir;

p = inputParser;
addParameter(p, 'Retest', -1, @isnumeric);
addParameter(p, 'Normalization', 'L');
addParameter(p, 'AdjointType', 'dA');
if(isfield(baseflow,'Mach')) 
    MaDefault = baseflow.Ma ; 
else
    MaDefault = 0.03; 
end;
addParameter(p,'Mach',MaDefault,@isnumeric); % Reynolds
addParameter(p,'ncores',1,@isnumeric);
addParameter(p, 'Adjoint','');
parse(p, varargin{:});


%% Position input files for FreeFem solver

SFcore_MoveDataFiles(baseflow.filename,'BaseFlow.txt')
SFcore_MoveDataFiles(eigenmode.filename,'Eigenmode.txt')

if isstruct(p.Results.Adjoint)
    SFcore_MoveDataFiles(p.Results.Adjoint.filename,'EigenmodeA.txt');
else
    % assuming that a file 'EigenmodeA.txt' already exists... in future we
    % should treat this as an error
    SF_core_log('nnn',' Warning in SF_WNL : better to explicitly specify an adjoint as SF_WNL(bf,em,''Adjoint'',emA,...)') 
end


%% Selects the right solver

switch(baseflow.mesh.problemtype)
    case('AxiXR')
    
    if (strcmp(ffdatadir, './DATA_SF_BIRDCALL_ERCOFTAC/') == 0) % in future we should manage this in a better way
        solvercommand = [ff, ' ', ffdir, 'WeaklyNonLinear_Axi.edp'];
    else
        solvercommand = [ff, ' ', ffdir, 'WeaklyNonLinear_BirdCall.edp'];
    end
    
    case('2D')
    solvercommand = ['echo ', p.Results.Normalization, ' ', p.Results.AdjointType, ' ', num2str(p.Results.Retest), ' | ', ff, ' ', ffdir, 'WeaklyNonLinear_2D.edp '];
    
    case('2DComp')
     solvercommand = ['echo '  p.Results.Normalization  ' ' num2str(p.Results.Retest) ' ' num2str(p.Results.Mach) ' | ' ff, ' ', ffdir, 'WeaklyNonLinear_2DComp.edp '  ];

    otherwise
        error(['Error in SF_WNL : not currently implemented for problemtype = ',baseflow.mesh.problemtype]);
end

%% Launching FF solver

errormessage = 'Error in SF_WNL.m';
mysystem(solvercommand, errormessage);



%% Importing results

wnl = SFcore_ImportData(baseflow.mesh, 'WNL_results.ff2m');

if (nargout >= 3 && p.Results.Retest > -1)
    meanflow = SFcore_ImportData(baseflow.mesh, 'MeanFlow_guess.ff2m');
    selfconsistentmode = SFcore_ImportData(baseflow.mesh, 'HBMode1_guess.ff2m');
    SF_core_log('n',' Estimating base flow and quasilinear mode from WNL')
    SF_core_log('n',['### Mode characteristics : AE = ', num2str(selfconsistentmode.AEnergy), ' ; Fy = ', num2str(selfconsistentmode.Fy), ' ; omega = ', num2str(imag(selfconsistentmode.lambda))]);
    SF_core_log('n',['### Mean-flow : Fx = ', num2str(meanflow.Fx)]);
end
if (nargout == 4 && p.Results.Retest > -1)
    secondharmonicmode = SFcore_ImportData(baseflow.mesh, 'HBMode2_guess.ff2m');
    SF_core_log('n',' Estimating SECOND HARMONIC mode from WNL')
    SF_core_log('n',['### Mode characteristics :  Fx = ', num2str(secondharmonicmode.Fx)]);
    
end
