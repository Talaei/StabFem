function [] = mydisp(verbositylevel, string)
% 
 global sfopts
 verbosity = sfopts.verbosity-1;
% if ~isempty(sfopts)
%     if verbositylevel < 5
%         loglevel = 'e';
%     elseif verbositylevel < 10
%         loglevel = 'w';
%     elseif verbositylevel < 30
%         loglevel = 'n';
%     elseif verbositylevel < 50
%         loglevel = 'nn';
%     elseif verbositylevel < 80
%         loglevel = 'd';
%     else
%         loglevel = 'dd';
%     end
%     SF_core_log('d', 'USE OF LEGACY FUNCTION DETECTED:');
%     SF_core_log('d', sprintf('Please replace legacy command "mydisp(%i,''%s'')"',verbositylevel,string));
%     SF_core_log('d', sprintf('By new command "SF_core_log(''%s'',''%s'')"',loglevel,string));
%     SF_core_log(loglevel, string);
%     return;
% end

%global ff ffdir ffdatadir sfdir verbosity
if (verbosity >= verbositylevel)
    disp([blanks(verbositylevel), string])
end
end