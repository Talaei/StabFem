% Example of using StabFem for flow around a confined square 
%
% This case uses a half-mesh.


%% Initialization 
run('../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures


%% Computation of a base flow and adapted mesh

BETA = 0.8;
Lmax = 20;
ffmesh = SF_Mesh('Mesh_SquareConfined.edp','Params',[BETA -5 Lmax],'problemtype','2D');
bf=SF_BaseFlow(ffmesh,'Re',1);

for Re = [10 50 70 100 150]
    bf=SF_BaseFlow(bf,'Re',Re);
end

bf=SF_BaseFlow(bf,'Re',150);
[ev,em] = SF_Stability(bf,'nev',1,'shift',0.,'type','D'); 
bf = SF_Adapt(bf,em);

bf=SF_BaseFlow(bf,'Re',250);
[ev,em] = SF_Stability(bf,'nev',1,'shift',0.3,'type','D'); 
bf = SF_Adapt(bf,em);

%% Plot baseflow and eigenmode

figure;SF_Plot(bf,'mesh');
figure;SF_Plot(bf,'ux','xlim',[-2 4],'ylim',[0,1],'title','Base flow for Re = 250');
figure;SF_Plot(em,'ux','xlim',[-2 4],'ylim',[0,1],'title',['Eigenmode for Re = 250 ; lambda = ',num2str(ev)]);

pause(0.1);


%% Plot full spectrum
[ev,em] = SF_Stability(bf,'nev',30,'shift',0.3+4i,'type','D','plotspectrum','yes'); 

pause(0.1);






%% Loop over Re for stability computations

Re_TAB = [250:-10:80];
evS_TAB = [];
shift = 0.21;
for Re = Re_TAB
    bf=SF_BaseFlow(bf,'Re',Re);
    if(Re==Re_TAB(1)) sort ='LR' ; else sort = 'cont'; end   % for first computation sort by increasing real part ; for next ones sort by continuation
    [evS,eM] = SF_Stability(bf,'nev',10,'shift',shift,'sort',sort,'sym','A');
    evS_TAB = [evS_TAB,evS];
end  

evI_TAB = [];
shift = 0.28+0.34i;
for Re = Re_TAB
    bf=SF_BaseFlow(bf,'Re',Re);
    if(Re==Re_TAB(1)) sort ='LR' ; else sort = 'cont'; end
    [evI,emI] = SF_Stability(bf,'nev',10,'shift',shift,'sort',sort,'sym','A');
    evI_TAB = [evI_TAB,evI];
end  

%% Plot stability branches

figure(10);
for j = 1:4
    plot(Re_TAB,real(evS_TAB(j,:)),'-');hold on;
    plot(Re_TAB,real(evI_TAB(j,:)),'--'); hold on;
end
plot(Re_TAB,0*Re_TAB,'k:')
xlabel('Re' ); ylabel('\lambda_r')
ylim([-.1,.35]);

figure(11);
plot(Re_TAB,imag(10*abs(evS_TAB(1,:))),'-');hold on;    
plot(Re_TAB,imag(evI_TAB(1,:)),'--'); hold on;

xlabel('Re' ); ylabel('\lambda_i ; 10 \lambda_i')
pause(0.1);



