function h = SF_Plot_ETA(eigenmode,varargin)
%
% This functions adds to the current plot a representation of the
% free-surface displacement ETA.
%
% Usage (just after SF_Plot(em,'ux')
% SF_Plot(em,['E',E,'style',color,'LineWidth',lw,'projection','n'|'r'|'z',symmmetry,'sym')
%
% With argument 'dim','3D' the function will draw a 3D surface.
%


p = inputParser;
addParameter(p,'dim','2D');
addParameter(p,'Amp',.15,@isnumeric);
addParameter(p,'style','r',@ischar); % style for plots (e.g. color)
addParameter(p,'LineWidth',2,@isnum); % Linewidth
addParameter(p,'projection','n',@ischar); % projection : 'n' | 'r' | 'z'
addParameter(p,'symmetry','no',@ischar); % symmetry condition. 
                                         % available values are 'no', 
                                         % 'YS' (symmetric with respect to Y axis)
                                         % 'YA' (antisymmetric with respect to Y axis) 
parse(p,varargin{:});

E = p.Results.Amp;
ffmesh = eigenmode.mesh;

if strcmpi(p.Results.dim,'2D')
% 2D plot

switch(p.Results.projection)
    case('n')
    h = plot(ffmesh.rsurf+real(E*eigenmode.eta).*ffmesh.N0r,ffmesh.zsurf+real(E*eigenmode.eta).*ffmesh.N0z,p.Results.style,'LineWidth',p.Results.LineWidth);
    case('r')
    h = plot(ffmesh.rsurf+real(E*eigenmode.eta)./ffmesh.N0r,p.Results.style,'LineWidth',p.Results.LineWidth);
    case('z')
    h = plot(ffmesh.rsurf,ffmesh.zsurf+real(E*eigenmode.eta)./ffmesh.N0z,p.Results.style,'LineWidth',p.Results.LineWidth);
end
        


switch p.Results.symmetry
    case('no')
        mydisp(15,'No symmetry');
    case('YS')
        eigenmodeSYM = eigenmode;
        eigenmodeSYM.mesh.rsurf = - eigenmodeSYM.mesh.rsurf;
        hold on; 
        SF_Plot_ETA(eigenmodeSYM,varargin{:},'symmetry','no');
        %
        %hold on; 
        %h1 = plot(-ffmesh.rsurf-real(E*eigenmode.eta).*ffmesh.N0r,ffmesh.zsurf+real(E*eigenmode.eta).*ffmesh.N0z,p.Results.style,'LineWidth',p.Results.LineWidth);
        %h = [h; h1];
    case('YA')
        %hold on; 
        %h1 = plot(-ffmesh.rsurf+real(E*eigenmode.eta).*ffmesh.N0r,ffmesh.zsurf-real(E*eigenmode.eta).*ffmesh.N0z,p.Results.style,'LineWidth',p.Results.LineWidth);
        %h = [h; h1];
        eigenmodeSYM = eigenmode;
        eigenmodeSYM.mesh.rsurf = - eigenmodeSYM.mesh.rsurf;
        eigenmodeSYM.eta = - eigenmodeSYM.eta;
        hold on; 
        SF_Plot_ETA(eigenmodeSYM,varargin{:},'symmetry','no');
end

else
% 3D plot

nphi = 40;
for k=1:nphi+1;
    phik = 2*pi*k/nphi;
    Rsurf = ffmesh.rsurf+real(E*exp(1i*eigenmode.m*phik)*eigenmode.eta).*ffmesh.N0r;
    Xsurf(k,:) = Rsurf*cos(phik);
    Ysurf(k,:) = Rsurf*sin(phik);    
    Zsurf(k,:) = ffmesh.zsurf+real(E*exp(1i*eigenmode.m*phik)*eigenmode.eta).*ffmesh.N0z;
    Csurf(k,:) = real(E*exp(1i*eigenmode.m*phik)*eigenmode.eta);
end

surf(Xsurf,Ysurf,Zsurf,Csurf);


end
