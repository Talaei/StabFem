% ================================================
% ==== AUTORUN SCRIPT USING SF_CORE FUNCTIONS ====
% ================================================

% ---------------------------
% -- LIST OF AUTORUN CASES --
% ---------------------------

AUTORUN_LIST.short = {'STABLE_CASES/EXAMPLE_Lshape',...
    'STABLE_CASES/CYLINDER','STABLE_CASES/SLOSHING'};

AUTORUN_LIST.long = {'STABLE_CASES/EXAMPLE_Lshape', 'STABLE_CASES/CYLINDER',...
    'STABLE_CASES/ROTATING_POLYGONS','STABLE_CASES/LiquidBridges',...
    'STABLE_CASES/BLUNTBODY_IN_PIPE','STABLE_CASES/SLOSHING'};

AUTORUN_LIST.cylinder = {'STABLE_CASES/CYLINDER'};

%%
% ------------------
% -- SETTING PATH --
% ------------------
addpath('../SOURCES_MATLAB/'); % 
%%
% ------------------------------
% -- DEFINING STABFEM OPTIONS --
% ------------------------------
% The structure of the current section copies that of SF_core_generateopts
% but does not require user-machine interactions. In case of doubt about
% expected options, the autorun exits with an status code = 1.

global sfopts
%[sfopts] = SF_core_init_autorun()
% note from DAVID : I put this in a function at the end ; 
% maybe it is redundant with something else ?
% % - Initializing sfopts -
if ~isempty(sfopts)
    warning('This autorun script expects sfopts to be empty. Its current content will be removed.')
end
sfopts = struct();

% - Testing the current environment and defining sfopts -
sfopts.verbosity = uint8(3);

if ~isunix || ismac
    warning('This autorun is meant to run on a linux platform.')
    1
    %exit(1)
end
sfopts.platform = 'linux';

if logical(exist('OCTAVE_VERSION', 'builtin'))
    warning('For now, this autorun expects to run on matlab, not octave.');
    1
    %exit(1)
end
sfopts.isoctave = false;

[s,~] = system('git --version');
if s==0
    sfopts.gitavailable = true;
end

if sfopts.gitavailable
    [s,~] = system('git rev-parse --is-inside-work-tree');
    if s==0
        sfopts.gitrepository = true;
    end
end

if sfopts.gitavailable
    [s,t] = system('git rev-parse --show-toplevel');
    if s==0
        sfroot = t(1:end-1);
    end
else
    % Careful: this sfroot definition only works if current directory
    % is AUTORUN.
    sfroot = [pwd '/..'];
end
sfopts.sfroot = sfroot;
if(ismac)
    s=0;
    t='/usr/local/ff++/openmpi-2.1/3.61-1/bin/FreeFem++'
else
    [s,t] = SF_core_syscommand('which', 'FreeFem++');
end
if s==0
    [ffroot,~,~] = fileparts(t);
   %if strcmp(ffroot(end-3:end),'/bin')
   %     ffroot = ffroot(1:end-4);
   % else
   %     ffroot = [ffroot '/../'];
   % end
   % David : removing the  assumption that FreeFem++ is in a 'bin' subfolder 
else
    warning('This autorun expects FreeFem++ binary to be in the current path.')
    %exit(1)
end
sfopts.ffroot = ffroot;

% a number of other options to recast   
sfopts.ffarg = '-nw -v 0';
sfopts.ffargDEBUG = '-v 1000';

 sfopts.ffdir = [sfopts.sfroot '/SOURCES_FreeFem/']; 

% SF_core_opts('write'); % Better not to do that ?

%%
% --------------------------
% -- SELECTING AN AUTORUN --
% --------------------------

% Pulling StabFem AutoRun Version from env. variables.
sfarver = getenv('SFARVER');
if isempty(sfarver)
    sfarver='short'; 
end

if ~isfield(AUTORUN_LIST,sfarver)
    warning('The requested autorun is not defined.')
    exit(1)
end

case_list = AUTORUN_LIST.(sfarver);

%%
% ----------------------
% -- RUNNING AUTORUNS --
% ----------------------

numsuccess = 0;
numfailure = 0;
numerror = 0;

cd('..')
origindir = pwd;

list_status = zeros(1,numel(case_list));
list_stacks = cell(1,numel(case_list));

for i=1:numel(case_list)
    SF_core_log('n', sprintf('\nAUTORUN test number %i: %s\n',i,case_list{i}), true);
    
    if strcmp(case_list{i},'STABLE_CASES/EXAMPLE_Lshape') % bidouille DAVID
            sfopts.ffdatadir = './';
            %ffdatadir = './';
    else
            %ffdatadir = './WORK/';
            sfopts.ffdatadir = './WORK/';
    end
    
    try
        cd(case_list{i});        
        a = autorun();
        if a==0
            numsuccess = numsuccess + 1;
            list_status(i) = 0;
        else
            numfailure = numfailure + 1;
            list_status(i) = 1;
        end
    catch err
        numerror = numerror + 1;
        list_status(i) = 2;
        list_stacks{i} = getReport(err);
    end
    cd(origindir)
end

% ---------------------------
% -- SHOWING FINAL RESULTS --
% ---------------------------
SF_core_log('n', sprintf('\n== AUTORUN SUMMARY ==\n'), true);
SF_core_log('n', sprintf('NUMBER OF SUCCESSES: %i', numsuccess), true);
SF_core_log('n', sprintf('NUMBER OF FAILURES: %i', numfailure), true);
SF_core_log('n', sprintf('NUMBER OF FATAL ERRORS: %i', numerror), true);

SF_core_log('n', sprintf('\n== AUTORUN DETAILS ==\n'), true);
for i=1:numel(case_list)
    switch list_status(i)
        case 0
            msg = 'SUCCESS';
        case 1
            msg = 'FAILURE';
        case 2
            msg = 'ERROR';
    end
    SF_core_log('n', sprintf('\t%s: %s', case_list{i}, msg), true);
    switch list_status(i)
        case 2
            SF_core_log('n', list_stacks{i}, true);
            
        otherwise
    end
end

if numfailure>0 || numerror>0
1
%exit(1)
else
0
%exit(0)
end


%function [sfopts] = SF_core_init_autorun()
% 
% % - Initializing sfopts -
% if ~isempty(sfopts)
%     warning('This autorun script expects sfopts to be empty. Its current content will be removed.')
% end
% sfopts = struct();
% 
% % - Testing the current environment and defining sfopts -
% sfopts.verbosity = uint8(3);
% 
% if ~isunix || ismac
%     warning('This autorun is meant to run on a linux platform.')
%     1
%     %exit(1)
% end
% sfopts.platform = 'linux';
% 
% if logical(exist('OCTAVE_VERSION', 'builtin'))
%     warning('For now, this autorun expects to run on matlab, not octave.');
%     1
%     %exit(1)
% end
% sfopts.isoctave = false;
% 
% [s,~] = system('git --version');
% if s==0
%     sfopts.gitavailable = true;
% end
% 
% if sfopts.gitavailable
%     [s,~] = system('git rev-parse --is-inside-work-tree');
%     if s==0
%         sfopts.gitrepository = true;
%     end
% end
% 
% if sfopts.gitavailable
%     [s,t] = system('git rev-parse --show-toplevel');
%     if s==0
%         sfroot = t(1:end-1);
%     end
% else
%     % Careful: this sfroot definition only works if current directory
%     % is AUTORUN.
%     sfroot = [pwd '/..'];
% end
% sfopts.sfroot = sfroot;
% if(ismac)
%     s=0;
%     t='/usr/local/ff++/openmpi-2.1/3.61-1';
% else
%     [s,t] = SF_core_syscommand('which', 'FreeFem++');
% end
% if s==0
%     [ffroot,~,~] = fileparts(t);
%     if strcmp(ffroot(end-3:end),'/bin')
%         ffroot = ffroot(1:end-4);
%     else
%         ffroot = [ffroot '/../'];
%     end
% else
%     warning('This autorun expects FreeFem++ binary to be in the current path.')
%     %exit(1)
%     1
% end
% sfopts.ffroot = ffroot;
% 
% sfopts.ffarg = '-nw -v 0';
% sfopts.ffargDEBUG = '-v 1000';
%
%end
